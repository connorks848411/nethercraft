package com.legacy.nethercraft.client;

import com.legacy.nethercraft.block.NetherBlocks;
import com.legacy.nethercraft.client.render.NetherTileEntityRendering;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.RenderTypeLookup;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;

public class NethercraftClient
{

	public static void initialization(FMLClientSetupEvent event)
	{
		NetherEntityRendering.init();
		NetherTileEntityRendering.init();
		
		RenderTypeLookup.setRenderLayer(NetherBlocks.glowood_leaves, RenderType.getCutoutMipped());
		renderCutout(NetherBlocks.purple_glowshroom);
		renderCutout(NetherBlocks.green_glowshroom);
		renderCutout(NetherBlocks.lava_reeds);
		renderCutout(NetherBlocks.dark_wheat);
		renderCutout(NetherBlocks.glowood_ladder);
		renderCutout(NetherBlocks.heat_glass);
		renderCutout(NetherBlocks.heat_glass_pane);
		renderCutout(NetherBlocks.soul_glass);
		renderCutout(NetherBlocks.soul_glass_pane);
		renderCutout(NetherBlocks.glowood_sapling);
		renderCutout(NetherBlocks.glowood_door);
		renderCutout(NetherBlocks.charcoal_torch);
		renderCutout(NetherBlocks.charcoal_wall_torch);
		renderCutout(NetherBlocks.foulite_torch);
		renderCutout(NetherBlocks.foulite_wall_torch);
		renderCutout(NetherBlocks.potted_glowood_sapling);
		renderCutout(NetherBlocks.potted_purple_glowshroom);
		renderCutout(NetherBlocks.potted_green_glowshroom);
	}
	
	private static void renderCutout(Block block)
	{
		RenderTypeLookup.setRenderLayer(block, RenderType.getCutout());
	}
}