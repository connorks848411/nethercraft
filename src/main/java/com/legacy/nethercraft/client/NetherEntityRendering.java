package com.legacy.nethercraft.client;

import com.legacy.nethercraft.client.render.entity.BloodyZombieRenderer;
import com.legacy.nethercraft.client.render.entity.CamouflageSpiderRenderer;
import com.legacy.nethercraft.client.render.entity.DarkZombieRenderer;
import com.legacy.nethercraft.client.render.entity.GhastBombRenderer;
import com.legacy.nethercraft.client.render.entity.ImpRenderer;
import com.legacy.nethercraft.client.render.entity.LavaBoatRenderer;
import com.legacy.nethercraft.client.render.entity.LavaSlimeRenderer;
import com.legacy.nethercraft.client.render.entity.NetherPaintingRenderer;
import com.legacy.nethercraft.client.render.entity.SlimeEggRenderer;
import com.legacy.nethercraft.client.render.entity.projectile.LiniumArrowRenderer;
import com.legacy.nethercraft.client.render.entity.projectile.NeridiumArrowRenderer;
import com.legacy.nethercraft.client.render.entity.projectile.NetherrackArrowRenderer;
import com.legacy.nethercraft.client.render.entity.projectile.PyridiumArrowRenderer;
import com.legacy.nethercraft.client.render.entity.tribal.TribalArcherRenderer;
import com.legacy.nethercraft.client.render.entity.tribal.TribalTraineeRenderer;
import com.legacy.nethercraft.client.render.entity.tribal.TribalWarriorRenderer;
import com.legacy.nethercraft.entity.NetherEntityTypes;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraftforge.fml.client.registry.IRenderFactory;
import net.minecraftforge.fml.client.registry.RenderingRegistry;

public class NetherEntityRendering
{

	public static void init()
	{
		register(NetherEntityTypes.DARK_ZOMBIE, DarkZombieRenderer::new);
		register(NetherEntityTypes.BLOODY_ZOMBIE, BloodyZombieRenderer::new);
		register(NetherEntityTypes.CAMOUFLAGE_SPIDER, CamouflageSpiderRenderer::new);
		register(NetherEntityTypes.IMP, ImpRenderer::new);
		register(NetherEntityTypes.LAVA_SLIME, LavaSlimeRenderer::new);

		register(NetherEntityTypes.TRIBAL_WARRIOR, TribalWarriorRenderer::new);
		register(NetherEntityTypes.TRIBAL_ARCHER, TribalArcherRenderer::new);
		register(NetherEntityTypes.TRIBAL_TRAINEE, TribalTraineeRenderer::new);

		register(NetherEntityTypes.NETHERRACK_ARROW, NetherrackArrowRenderer::new);
		register(NetherEntityTypes.NERIDIUM_ARROW, NeridiumArrowRenderer::new);
		register(NetherEntityTypes.PYRIDIUM_ARROW, PyridiumArrowRenderer::new);
		register(NetherEntityTypes.LINIUM_ARROW, LiniumArrowRenderer::new);

		register(NetherEntityTypes.SLIME_EGGS, SlimeEggRenderer::new);
		register(NetherEntityTypes.GHAST_BOMB, GhastBombRenderer::new);
		register(NetherEntityTypes.NETHER_PAINTING, NetherPaintingRenderer::new);
		register(NetherEntityTypes.LAVA_BOAT, LavaBoatRenderer::new);
	}

	private static <T extends Entity> void register(EntityType<T> entityClass, IRenderFactory<? super T> renderFactory)
	{
		RenderingRegistry.registerEntityRenderingHandler(entityClass, renderFactory);
	}
}