package com.legacy.nethercraft.client.render.entity;

import com.legacy.nethercraft.entity.misc.GhastBombEntity;
import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.block.Blocks;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.Vector3f;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.TNTMinecartRenderer;
import net.minecraft.client.renderer.texture.AtlasTexture;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class GhastBombRenderer extends EntityRenderer<GhastBombEntity>
{

	public GhastBombRenderer(EntityRendererManager renderManagerIn)
	{
		super(renderManagerIn);
		this.shadowSize = 0.5F;
	}

	@Override
	public void render(GhastBombEntity p_225623_1_, float p_225623_2_, float p_225623_3_, MatrixStack p_225623_4_, IRenderTypeBuffer p_225623_5_, int p_225623_6_)
	{
		p_225623_4_.push();
		p_225623_4_.translate(0.0D, 0.5D, 0.0D);

		if ((float) p_225623_1_.getFuse() - p_225623_3_ + 1.0F < 10.0F)
		{
			float f = 1.0F - ((float) p_225623_1_.getFuse() - p_225623_3_ + 1.0F) / 10.0F;
			f = MathHelper.clamp(f, 0.0F, 1.0F);
			f = f * f;
			f = f * f;
			float f1 = 1.0F + f * 0.3F;
			p_225623_4_.scale(f1, f1, f1);
		}

		p_225623_4_.rotate(Vector3f.YP.rotationDegrees(-90.0F));
		p_225623_4_.translate(-0.5D, -0.5D, 0.5D);
		p_225623_4_.rotate(Vector3f.YP.rotationDegrees(90.0F));
		TNTMinecartRenderer.renderTntFlash(Blocks.TNT.getDefaultState(), p_225623_4_, p_225623_5_, p_225623_6_, p_225623_1_.getFuse() / 5 % 2 == 0);
		p_225623_4_.pop();
		super.render(p_225623_1_, p_225623_2_, p_225623_3_, p_225623_4_, p_225623_5_, p_225623_6_);
	}

	@SuppressWarnings("deprecation")
	@Override
	public ResourceLocation getEntityTexture(GhastBombEntity entity)
	{
		return AtlasTexture.LOCATION_BLOCKS_TEXTURE;
	}
}