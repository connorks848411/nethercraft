package com.legacy.nethercraft.client.render.model;

import com.google.common.collect.ImmutableList;

import net.minecraft.client.renderer.entity.model.SegmentedModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.MathHelper;

public class ImpModel<T extends Entity> extends SegmentedModel<T>
{
	public ModelRenderer rightLeg;
	public ModelRenderer leftLeg;
	public ModelRenderer body;
	public ModelRenderer leftArm;
	public ModelRenderer rightArm;
	public ModelRenderer head;
	public ModelRenderer tail;
	public ModelRenderer righthorn;
	public ModelRenderer lefthorn;
	public ModelRenderer snout;

	public ImpModel()
	{
		this.textureWidth = 64;
		this.textureHeight = 32;
		this.lefthorn = new ModelRenderer(this, 0, 2);
		this.lefthorn.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.lefthorn.addBox(2.0F, -9.0F, -3.0F, 2, 1, 1, 0.0F);
		this.snout = new ModelRenderer(this, 36, 8);
		this.snout.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.snout.addBox(-2.0F, -4.0F, -5.0F, 4, 4, 1, 0.0F);
		this.leftArm = new ModelRenderer(this, 32, 0);
		this.leftArm.setRotationPoint(-5.0F, 10.5F, 0.0F);
		this.leftArm.addBox(-2.0F, 0.0F, -1.0F, 2, 6, 2, 0.0F);
		this.leftLeg = new ModelRenderer(this, 0, 16);
		this.leftLeg.setRotationPoint(-2.9F, 18.0F, 0.0F);
		this.leftLeg.addBox(-2.0F, 0.0F, -2.0F, 4, 6, 4, 0.0F);
		this.body = new ModelRenderer(this, 16, 16);
		this.body.setRotationPoint(0.0F, 19.0F, 0.0F);
		this.body.addBox(-5.0F, -2.5F, 0.0F, 10, 5, 9, 0.0F);
		this.setRotateAngle(body, 1.5707963267948966F, 0.0F, 0.0F);
		this.rightArm = new ModelRenderer(this, 32, 0);
		this.rightArm.mirror = true;
		this.rightArm.setRotationPoint(5.0F, 10.5F, 0.0F);
		this.rightArm.addBox(0.0F, 0.0F, -1.0F, 2, 6, 2, 0.0F);
		this.tail = new ModelRenderer(this, 0, 22);
		this.tail.setRotationPoint(0.0F, 2.5F, 1.0F);
		this.tail.addBox(0.0F, 0.0F, 0.0F, 0, 4, 4, 0.0F);
		this.setRotateAngle(tail, 0.6283185307179586F, 0.4363323129985824F, 1.3089969389957472F);
		this.righthorn = new ModelRenderer(this, 0, 0);
		this.righthorn.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.righthorn.addBox(-4.0F, -9.0F, -3.0F, 2, 1, 1, 0.0F);
		this.head = new ModelRenderer(this, 0, 0);
		this.head.setRotationPoint(0.0F, 10.0F, 0.0F);
		this.head.addBox(-5.0F, -8.0F, -4.0F, 10, 8, 8, 0.0F);
		this.rightLeg = new ModelRenderer(this, 0, 16);
		this.rightLeg.mirror = true;
		this.rightLeg.setRotationPoint(2.9F, 18.0F, 0.0F);
		this.rightLeg.addBox(-2.0F, 0.0F, -2.0F, 4, 6, 4, 0.0F);
		this.head.addChild(this.lefthorn);
		this.head.addChild(this.snout);
		this.body.addChild(this.tail);
		this.head.addChild(this.righthorn);
	}

	@Override
	public Iterable<ModelRenderer> getParts()
	{
		return ImmutableList.of(this.head, this.body, this.rightLeg, this.leftLeg, this.leftArm, this.rightArm);
	}

	@Override
	public void setRotationAngles(T entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch)
	{
		this.head.rotateAngleX = headPitch / 57.29578F;
		this.head.rotateAngleY = netHeadYaw / 57.29578F;

		this.leftLeg.rotateAngleX = MathHelper.cos((float) (limbSwing * 0.6662F + Math.PI)) * 1.4F * limbSwingAmount;
		this.rightLeg.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;

		this.leftArm.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F) * 2.0F * limbSwingAmount * 0.5F;
		this.rightArm.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F + 3.141593F) * 2.0F * limbSwingAmount * 0.5F;
	}

	public void setRotateAngle(ModelRenderer modelRenderer, float x, float y, float z)
	{
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}