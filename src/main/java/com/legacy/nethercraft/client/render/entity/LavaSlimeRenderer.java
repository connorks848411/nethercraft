package com.legacy.nethercraft.client.render.entity;

import com.legacy.nethercraft.Nethercraft;
import com.legacy.nethercraft.entity.LavaSlimeEntity;
import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.client.renderer.entity.layers.SlimeGelLayer;
import net.minecraft.client.renderer.entity.model.SlimeModel;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class LavaSlimeRenderer extends MobRenderer<LavaSlimeEntity, SlimeModel<LavaSlimeEntity>>
{
	private static final ResourceLocation ORANGE_SLIME_TEXTURE = Nethercraft.locate("textures/entity/slime/orange_slime.png");

	private static final ResourceLocation RED_SLIME_TEXTURE = Nethercraft.locate("textures/entity/slime/red_slime.png");

	public LavaSlimeRenderer(EntityRendererManager renderManagerIn)
	{
		super(renderManagerIn, new SlimeModel<>(16), 0.25F);
		this.addLayer(new SlimeGelLayer<>(this));
	}

	@Override
	public void render(LavaSlimeEntity entityIn, float entityYaw, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn)
	{
		this.shadowSize = 0.25F * (float) entityIn.getSlimeSize();
		super.render(entityIn, entityYaw, partialTicks, matrixStackIn, bufferIn, packedLightIn);
	}

	@Override
	protected void preRenderCallback(LavaSlimeEntity slime, MatrixStack stack, float tick)
	{
		stack.scale(0.999F, 0.999F, 0.999F);
		stack.translate(0.0D, (double) 0.001F, 0.0D);
		float f1 = (float) slime.getSlimeSize();
		float f2 = MathHelper.lerp(tick, slime.prevSquishFactor, slime.squishFactor) / (f1 * 0.5F + 1.0F);
		float f3 = 1.0F / (f2 + 1.0F);
		stack.scale(f3 * f1, 1.0F / f3 * f1, f3 * f1);
	}

	@Override
	public ResourceLocation getEntityTexture(LavaSlimeEntity entity)
	{
		return ((LavaSlimeEntity) entity).getDataManager().get(LavaSlimeEntity.SLIME_TYPE) == 1 ? RED_SLIME_TEXTURE : ORANGE_SLIME_TEXTURE;
	}
}