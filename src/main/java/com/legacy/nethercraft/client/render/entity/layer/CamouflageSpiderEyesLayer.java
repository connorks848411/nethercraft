package com.legacy.nethercraft.client.render.entity.layer;

import com.legacy.nethercraft.Nethercraft;

import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.IEntityRenderer;
import net.minecraft.client.renderer.entity.layers.AbstractEyesLayer;
import net.minecraft.client.renderer.entity.model.SpiderModel;
import net.minecraft.entity.Entity;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class CamouflageSpiderEyesLayer<T extends Entity, M extends SpiderModel<T>> extends AbstractEyesLayer<T, M>
{
	private static final RenderType EYES = RenderType.getEyes(Nethercraft.locate("textures/entity/spider/eyes.png"));

	public CamouflageSpiderEyesLayer(IEntityRenderer<T, M> renderer)
	{
		super(renderer);
	}

	@Override
	public RenderType getRenderType()
	{
		return EYES;
	}
}