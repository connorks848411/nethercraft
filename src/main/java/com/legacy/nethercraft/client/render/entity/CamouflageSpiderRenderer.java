package com.legacy.nethercraft.client.render.entity;

import com.legacy.nethercraft.Nethercraft;
import com.legacy.nethercraft.client.render.entity.layer.CamouflageSpiderEyesLayer;
import com.legacy.nethercraft.entity.CamouflageSpiderEntity;

import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.client.renderer.entity.model.SpiderModel;
import net.minecraft.util.ResourceLocation;

public class CamouflageSpiderRenderer<T extends CamouflageSpiderEntity> extends MobRenderer<T, SpiderModel<T>>
{

	private static final ResourceLocation NETHERRACK_TEXTURE = Nethercraft.locate("textures/entity/spider/spider_netherrack.png");

	private static final ResourceLocation DIRT_TEXTURE = Nethercraft.locate("textures/entity/spider/spider_nether_dirt.png");

	public CamouflageSpiderRenderer(EntityRendererManager renderManagerIn)
	{
		super(renderManagerIn, new SpiderModel<>(), 0.8F);
		this.addLayer(new CamouflageSpiderEyesLayer<>(this));
	}

	protected float getDeathMaxRotation(T entityLivingBaseIn)
	{
		return 180.0F;
	}

	@Override
	public ResourceLocation getEntityTexture(T entity)
	{
		return entity.getDataManager().get(CamouflageSpiderEntity.SPIDER_TYPE) == 1 ? NETHERRACK_TEXTURE : DIRT_TEXTURE;
	}
}