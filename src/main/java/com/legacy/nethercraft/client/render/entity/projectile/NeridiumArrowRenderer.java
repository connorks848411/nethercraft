package com.legacy.nethercraft.client.render.entity.projectile;

import com.legacy.nethercraft.Nethercraft;
import com.legacy.nethercraft.entity.projectile.NeridiumArrowEntity;

import net.minecraft.client.renderer.entity.ArrowRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class NeridiumArrowRenderer extends ArrowRenderer<NeridiumArrowEntity>
{

	public static final ResourceLocation ARROW = Nethercraft.locate("textures/entity/projectile/neridium_arrow.png");

	public NeridiumArrowRenderer(EntityRendererManager manager)
	{
		super(manager);
	}

	public ResourceLocation getEntityTexture(NeridiumArrowEntity entity)
	{
		return ARROW;
	}
}