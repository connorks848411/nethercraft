package com.legacy.nethercraft.client.render.entity;

import com.legacy.nethercraft.Nethercraft;
import com.legacy.nethercraft.entity.misc.NetherPaintingEntity;

import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class NetherPaintingRenderer extends EntityRenderer<NetherPaintingEntity>
{

	// TODO: Add proper rendering lol
	public NetherPaintingRenderer(EntityRendererManager renderManagerIn)
	{
		super(renderManagerIn);
	}

	@Override
	public ResourceLocation getEntityTexture(NetherPaintingEntity entity)
	{
		return Nethercraft.locate("textures/entity/nether_paintings.png");
	}
}