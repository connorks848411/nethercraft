package com.legacy.nethercraft.client.render.entity;

import com.legacy.nethercraft.item.NetherItems;
import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.ItemRenderer;
import net.minecraft.client.renderer.Vector3f;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.texture.AtlasTexture;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.entity.Entity;
import net.minecraft.entity.IRendersAsItem;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@SuppressWarnings("deprecation")
@OnlyIn(Dist.CLIENT)
public class SlimeEggRenderer<T extends Entity & IRendersAsItem> extends EntityRenderer<T>
{
	private final ItemRenderer itemRenderer;
	private final float scale;
	private final boolean lit;

	public SlimeEggRenderer(EntityRendererManager manager)
	{
		super(manager);
		this.itemRenderer = Minecraft.getInstance().getItemRenderer();
		this.scale = 1.0F;
		this.lit = false;
	}

	@Override
	protected int getBlockLight(T entityIn, float partialTicks)
	{
		return this.lit ? 15 : super.getBlockLight(entityIn, partialTicks);
	}

	@Override
	public void render(T entityIn, float entityYaw, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn)
	{
		matrixStackIn.push();
		matrixStackIn.scale(this.scale, this.scale, this.scale);
		matrixStackIn.rotate(this.renderManager.getCameraOrientation());
		matrixStackIn.rotate(Vector3f.YP.rotationDegrees(180.0F));
		this.itemRenderer.renderItem(new ItemStack(NetherItems.slime_eggs), ItemCameraTransforms.TransformType.GROUND, packedLightIn, OverlayTexture.NO_OVERLAY, matrixStackIn, bufferIn);
		matrixStackIn.pop();
		super.render(entityIn, entityYaw, partialTicks, matrixStackIn, bufferIn, packedLightIn);
	}

	@Override
	public ResourceLocation getEntityTexture(Entity entity)
	{
		return AtlasTexture.LOCATION_BLOCKS_TEXTURE;
	}
}