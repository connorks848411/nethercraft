package com.legacy.nethercraft.item.util;

import com.legacy.nethercraft.item.NetherItems;

import net.minecraft.entity.LivingEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class NetherSoupItem extends Item
{

	public NetherSoupItem(Item.Properties p_i50054_1_)
	{
		super(p_i50054_1_);
	}

	public ItemStack onItemUseFinish(ItemStack stack, World worldIn, LivingEntity entityLiving)
	{
		super.onItemUseFinish(stack, worldIn, entityLiving);
		return new ItemStack(NetherItems.glowood_bowl);
	}
}