package com.legacy.nethercraft.item.util;

import com.legacy.nethercraft.block.NetherBlocks;
import com.legacy.nethercraft.item.NetherItems;

import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraftforge.event.furnace.FurnaceFuelBurnTimeEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class FuelHandler
{
	@SubscribeEvent
	public void getBurnTime(FurnaceFuelBurnTimeEvent event)
	{
		Item item = event.getItemStack().getItem();

		if (item.equals(NetherItems.neridium_lava_bucket))
			event.setBurnTime(20000);

		if (item.equals(NetherItems.glowood_stick))
			event.setBurnTime(100);
		
		if (item.equals(NetherItems.foulite_dust))
			event.setBurnTime(4800);

		if (item.equals(Items.GLOWSTONE_DUST))
			event.setBurnTime(2400);

		//else if (item.equals(NetherBlocks.glowood_fence_gate.asItem()))
			//event.setBurnTime(300);
		
		else if (item.equals(NetherBlocks.glowood_sapling.asItem()))
			event.setBurnTime(150);

		else if (item.equals(NetherBlocks.glowood_fence.asItem()))
			event.setBurnTime(300);
		
		else if (item.equals(NetherBlocks.glowood_planks.asItem()))
			event.setBurnTime(300);
		else if (item.equals(NetherBlocks.glowood_ladder.asItem()))
			event.setBurnTime(300);
		else if (item.equals(NetherBlocks.glowood_crafting_table.asItem()))
			event.setBurnTime(300);
		else if (item.equals(NetherBlocks.glowood_bookshelf.asItem()))
			event.setBurnTime(300);
		else if (item.equals(NetherItems.glowood_sword) || item.equals(NetherItems.glowood_pickaxe) || item.equals(NetherItems.glowood_axe) || item.equals(NetherItems.glowood_shovel) || item.equals(NetherItems.glowood_hoe))
			event.setBurnTime(200);
	}
}
