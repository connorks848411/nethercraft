package com.legacy.nethercraft.item.util;

import net.minecraft.advancements.CriteriaTriggers;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.FireBlock;
import net.minecraft.block.NetherPortalBlock;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;

public class NeridiumLighterItem extends Item
{

	public NeridiumLighterItem(Item.Properties builder)
	{
		super(builder);
	}

	public ActionResultType onItemUse(ItemUseContext context)
	{
		PlayerEntity playerentity = context.getPlayer();
		IWorld iworld = context.getWorld();
		BlockPos blockpos = context.getPos();
		BlockPos blockpos1 = blockpos.offset(context.getFace());
		if (func_219996_a(iworld.getBlockState(blockpos1), iworld, blockpos1))
		{
			iworld.playSound(playerentity, blockpos1, SoundEvents.ITEM_FLINTANDSTEEL_USE, SoundCategory.BLOCKS, 1.0F, random.nextFloat() * 0.4F + 0.8F);
			for(BlockPos aroundPos : BlockPos.getAllInBoxMutable(blockpos1.add(-1, 0, -1), blockpos1.add(1, 0, 1)))
			{
				if (iworld.getBlockState(aroundPos).getBlock() == Blocks.AIR)
				{
					BlockState blockstate1 = ((FireBlock) Blocks.FIRE).getStateForPlacement(iworld, aroundPos);
					iworld.setBlockState(aroundPos, blockstate1, 11);
				}
			}
			
			ItemStack itemstack = context.getItem();

			if (playerentity instanceof ServerPlayerEntity)
			{
				CriteriaTriggers.PLACED_BLOCK.trigger((ServerPlayerEntity) playerentity, blockpos1, itemstack);
				itemstack.damageItem(1, playerentity, (p_219999_1_) ->
				{
					p_219999_1_.sendBreakAnimation(context.getHand());
				});
			}

			return ActionResultType.SUCCESS;
		}
		else
		{
			BlockState blockstate = iworld.getBlockState(blockpos);
			if (isUnlitCampfire(blockstate))
			{
				iworld.playSound(playerentity, blockpos, SoundEvents.ITEM_FLINTANDSTEEL_USE, SoundCategory.BLOCKS, 1.0F, random.nextFloat() * 0.4F + 0.8F);
				iworld.setBlockState(blockpos, blockstate.with(BlockStateProperties.LIT, Boolean.valueOf(true)), 11);
				if (playerentity != null)
				{
					context.getItem().damageItem(1, playerentity, (p_219998_1_) ->
					{
						p_219998_1_.sendBreakAnimation(context.getHand());
					});
				}
				return ActionResultType.SUCCESS;
			}
			else
			{
				return ActionResultType.FAIL;
			}
		}

	}

	public static boolean isUnlitCampfire(BlockState state)
	{
		return state.getBlock() == Blocks.CAMPFIRE && !state.get(BlockStateProperties.WATERLOGGED) && !state.get(BlockStateProperties.LIT);
	}

	@SuppressWarnings("deprecation")
	public static boolean func_219996_a(BlockState p_219996_0_, IWorld p_219996_1_, BlockPos p_219996_2_)
	{
		BlockState blockstate = ((FireBlock) Blocks.FIRE).getStateForPlacement(p_219996_1_, p_219996_2_);
		boolean flag = false;
		for (Direction direction : Direction.Plane.HORIZONTAL)
		{
			if (p_219996_1_.getBlockState(p_219996_2_.offset(direction)).getBlock() == Blocks.OBSIDIAN && ((NetherPortalBlock) Blocks.NETHER_PORTAL).isPortal(p_219996_1_, p_219996_2_) != null)
			{
				flag = true;
			}
		}
		return p_219996_0_.isAir() && (blockstate.isValidPosition(p_219996_1_, p_219996_2_) || flag);
	}
}