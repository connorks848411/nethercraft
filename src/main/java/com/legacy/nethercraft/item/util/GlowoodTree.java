package com.legacy.nethercraft.item.util;

import java.util.Random;

import com.legacy.nethercraft.world.NetherEvents;

import net.minecraft.block.trees.Tree;
import net.minecraft.world.gen.feature.ConfiguredFeature;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.TreeFeatureConfig;

public class GlowoodTree extends Tree
{
	@Override
	protected ConfiguredFeature<TreeFeatureConfig, ?> getTreeFeature(Random randomIn, boolean p_225546_2_)
	{
		return Feature.NORMAL_TREE.withConfiguration(NetherEvents.GLOWOOD_CONFIG);
	}
}