package com.legacy.nethercraft.item.util;

import net.minecraft.item.Food;

public class NetherFoods
{
	public static final Food DEVIL_BREAD = (new Food.Builder()).hunger(5).saturation(0.6F).build();

	public static final Food GLOW_APPLE = (new Food.Builder()).hunger(4).saturation(0.3F).build();

	public static final Food GLOW_STEW = (new Food.Builder()).hunger(6).saturation(0.6F).build();
}