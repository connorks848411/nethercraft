package com.legacy.nethercraft.item;

import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.item.ArrowItem;
import net.minecraft.item.BowItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.stats.Stats;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.world.World;

public class NetherBowItem extends BowItem
{

	public NetherBowItem(Item.Properties builder)
	{
		super(builder);
		this.addPropertyOverride(new ResourceLocation("pull"), (p_210310_0_, p_210310_1_, p_210310_2_) ->
		{
			if (p_210310_2_ == null)
			{
				return 0.0F;
			}
			else
			{
				return !(p_210310_2_.getActiveItemStack().getItem() instanceof BowItem) ? 0.0F : (float) (p_210310_0_.getUseDuration() - p_210310_2_.getItemInUseCount()) / this.getChargeDuration();
			}
		});
		this.addPropertyOverride(new ResourceLocation("pulling"), (p_210309_0_, p_210309_1_, p_210309_2_) ->
		{
			return p_210309_2_ != null && p_210309_2_.isHandActive() && p_210309_2_.getActiveItemStack() == p_210309_0_ ? 1.0F : 0.0F;
		});
	}

	@Override
	public void onPlayerStoppedUsing(ItemStack stack, World worldIn, LivingEntity entityLiving, int timeLeft)
	{
		if (entityLiving instanceof PlayerEntity)
		{
			PlayerEntity playerentity = (PlayerEntity) entityLiving;
			boolean flag = playerentity.abilities.isCreativeMode || EnchantmentHelper.getEnchantmentLevel(Enchantments.INFINITY, stack) > 0;
			ItemStack itemstack = playerentity.findAmmo(stack);
			int i = this.getUseDuration(stack) - timeLeft;
			i = net.minecraftforge.event.ForgeEventFactory.onArrowLoose(stack, worldIn, playerentity, i, !itemstack.isEmpty() || flag);
			if (i < 0)
				return;
			if (!itemstack.isEmpty() || flag)
			{
				if (itemstack.isEmpty())
				{
					itemstack = new ItemStack(Items.ARROW);
				}

				float maxVelocity = stack.getItem() == NetherItems.pyridium_bow ? 1.25F : stack.getItem() == NetherItems.linium_bow ? 1.5F : 1.0F;

				float f = getArrowVelocity(i, this.getChargeDuration()) * maxVelocity;
				if (!((double) f < 0.1D))
				{
					boolean flag1 = playerentity.abilities.isCreativeMode || (itemstack.getItem() instanceof ArrowItem && ((ArrowItem) itemstack.getItem()).isInfinite(itemstack, stack, playerentity));
					if (!worldIn.isRemote)
					{
						ArrowItem arrowitem = (ArrowItem) (itemstack.getItem() instanceof ArrowItem ? itemstack.getItem() : Items.ARROW);
						AbstractArrowEntity abstractarrowentity = arrowitem.createArrow(worldIn, itemstack, playerentity);
						abstractarrowentity = customeArrow(abstractarrowentity);

						if (stack.getItem() == NetherItems.netherrack_bow)
						{
							if (f >= 0.7F && stack.getItem() == NetherItems.netherrack_bow)
							{
								f = 0.7F;
								abstractarrowentity.setIsCritical(false);
							}
							abstractarrowentity.shoot(playerentity, playerentity.rotationPitch, playerentity.rotationYaw, 0.0F, f * 3.0F, 1.0F);
							int j = EnchantmentHelper.getEnchantmentLevel(Enchantments.POWER, stack) / 2;
							if (j > 0)
							{
								abstractarrowentity.setDamage(abstractarrowentity.getDamage() + (double) j * 0.5D + 0.5D);
							}
							int k = EnchantmentHelper.getEnchantmentLevel(Enchantments.PUNCH, stack) / 2;
							if (k > 0)
							{
								abstractarrowentity.setKnockbackStrength(k);
							}
							if (EnchantmentHelper.getEnchantmentLevel(Enchantments.FLAME, stack) > 0)
							{
								abstractarrowentity.setFire(50);
							}
							stack.damageItem(1, playerentity, (p_220009_1_) ->
							{
								p_220009_1_.sendBreakAnimation(playerentity.getActiveHand());
							});
							if (flag1 || playerentity.abilities.isCreativeMode && (itemstack.getItem() == Items.SPECTRAL_ARROW || itemstack.getItem() == Items.TIPPED_ARROW))
							{
								abstractarrowentity.pickupStatus = AbstractArrowEntity.PickupStatus.CREATIVE_ONLY;
							}
							worldIn.addEntity(abstractarrowentity);
						}
						else
						{
							if (f == maxVelocity)
							{
								abstractarrowentity.setIsCritical(true);
							}
							abstractarrowentity.shoot(playerentity, playerentity.rotationPitch, playerentity.rotationYaw, 0.0F, f * 3.0F, 1.0F);
							int j = EnchantmentHelper.getEnchantmentLevel(Enchantments.POWER, stack);
							if (j > 0)
							{
								abstractarrowentity.setDamage(abstractarrowentity.getDamage() + (double) j * 0.5D + 0.5D);
							}
							int k = EnchantmentHelper.getEnchantmentLevel(Enchantments.PUNCH, stack);
							if (k > 0)
							{
								abstractarrowentity.setKnockbackStrength(k);
							}
							if (EnchantmentHelper.getEnchantmentLevel(Enchantments.FLAME, stack) > 0)
							{
								abstractarrowentity.setFire(100);
							}
							stack.damageItem(1, playerentity, (p_220009_1_) ->
							{
								p_220009_1_.sendBreakAnimation(playerentity.getActiveHand());
							});
							if (flag1 || playerentity.abilities.isCreativeMode && (itemstack.getItem() == Items.SPECTRAL_ARROW || itemstack.getItem() == Items.TIPPED_ARROW))
							{
								abstractarrowentity.pickupStatus = AbstractArrowEntity.PickupStatus.CREATIVE_ONLY;
							}	
						}
						worldIn.addEntity(abstractarrowentity);
					}
					worldIn.playSound((PlayerEntity) null, playerentity.getPosition().getX(), playerentity.getPosition().getY(), playerentity.getPosition().getZ(), SoundEvents.ENTITY_ARROW_SHOOT, SoundCategory.PLAYERS, 1.0F, 1.0F / (random.nextFloat() * 0.4F + 1.2F) + f * 0.5F);
					if (!flag1 && !playerentity.abilities.isCreativeMode)
					{
						itemstack.shrink(1);
						if (itemstack.isEmpty())
						{
							playerentity.inventory.deleteStack(itemstack);
						}
					}
					playerentity.addStat(Stats.ITEM_USED.get(this));
				}
			}
		}
	}

	public static float getArrowVelocity(int charge, float chargeTime)
	{
		float f = (float) charge / chargeTime;
		f = (f * f + f * 2.0F) / 3.0F;
		if (f > 1.0F)
		{
			f = 1.0F;
		}
		return f;
	}

	@Override
	public int getUseDuration(ItemStack stack)
	{
		return stack.getItem() == NetherItems.linium_bow ? 144000 : 72000;
	}

	public float getChargeDuration()
	{
		return this.getItem() == NetherItems.linium_bow ? 40.0F : this.getItem() == NetherItems.pyridium_bow ? 30.0F : this.getItem() == NetherItems.neridium_bow ? 20.0F: 10.0F;
	}
}