package com.legacy.nethercraft.item;

import com.legacy.nethercraft.NethercraftRegistry;
import com.legacy.nethercraft.block.NetherBlocks;
import com.legacy.nethercraft.entity.NetherEntityTypes;
import com.legacy.nethercraft.item.arrow.NetherArrowItem;
import com.legacy.nethercraft.item.misc.LavaBoatItem;
import com.legacy.nethercraft.item.misc.SlimeEggItem;
import com.legacy.nethercraft.item.util.NeridiumBucketItem;
import com.legacy.nethercraft.item.util.NeridiumLighterItem;
import com.legacy.nethercraft.item.util.NetherArmorMaterial;
import com.legacy.nethercraft.item.util.NetherFoods;
import com.legacy.nethercraft.item.util.NetherItemGroup;
import com.legacy.nethercraft.item.util.NetherItemTier;
import com.legacy.nethercraft.item.util.NetherSoupItem;

import net.minecraft.entity.EntityType;
import net.minecraft.fluid.Fluids;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.AxeItem;
import net.minecraft.item.BlockItem;
import net.minecraft.item.BlockNamedItem;
import net.minecraft.item.BoneMealItem;
import net.minecraft.item.HangingEntityItem;
import net.minecraft.item.HoeItem;
import net.minecraft.item.Item;
import net.minecraft.item.PickaxeItem;
import net.minecraft.item.ShovelItem;
import net.minecraft.item.SpawnEggItem;
import net.minecraft.item.SwordItem;
import net.minecraft.item.WallOrFloorItem;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.registries.IForgeRegistry;

public class NetherItems
{
	private static IForgeRegistry<Item> iItemRegistry;

	public static Item pyridium_pickaxe, pyridium_shovel, pyridium_axe, pyridium_hoe, pyridium_sword;

	public static Item netherrack_pickaxe, netherrack_shovel, netherrack_axe, netherrack_hoe, netherrack_sword;

	public static Item glowood_pickaxe, glowood_shovel, glowood_axe, glowood_hoe, glowood_sword;

	public static Item linium_pickaxe, linium_shovel, linium_axe, linium_hoe, linium_sword;

	public static Item neridium_pickaxe, neridium_shovel, neridium_axe, neridium_hoe, neridium_sword;

	public static Item glowood_stick, foulite_dust, magma_caramel, imp_skin, ghast_rod, red_feather, lava_book,
			lava_paper;

	public static Item ghast_bones, ghast_marrow;

	public static Item neridium_ingot, pyridium_ingot, linium_ingot, w_dust, w_obsidian_ingot;

	public static Item lava_boat;

	public static Item dark_seeds, dark_wheat, lava_reeds;

	public static Item devil_bread, glow_stew, glow_apple;

	public static Item nether_painting;

	public static Item glowood_bowl;

	public static Item netherrack_bow, neridium_bow, pyridium_bow, linium_bow, netherrack_arrow, neridium_arrow,
			pyridium_arrow, linium_arrow;

	public static Item neridium_lighter, slime_eggs;

	public static Item imp_helmet, imp_chestplate, imp_leggings, imp_boots;

	public static Item neridium_helmet, neridium_chestplate, neridium_leggings, neridium_boots;

	public static Item w_obsidian_helmet, w_obsidian_chestplate, w_obsidian_leggings, w_obsidian_boots;

	public static Item neridium_bucket, neridium_lava_bucket;

	public static Item foulite_torch, charcoal_torch, glowood_chest;

	public static Item imp_spawn_egg, dark_zombie_spawn_egg, bloody_zombie_spawn_egg, lava_slime_spawn_egg,
			camouflage_spider_spawn_egg, tribal_warrior_spawn_egg, tribal_trainee_spawn_egg, tribal_archer_spawn_egg;

	public static void initialization(Register<Item> event)
	{
		NetherItems.iItemRegistry = event.getRegistry();
		registerBlockItems();

		imp_spawn_egg = register("imp_spawn_egg", new SpawnEggItem(NetherEntityTypes.IMP, 0x1f4d14, 0x8e3801, new Item.Properties().group(NetherItemGroup.TOOLS)));
		dark_zombie_spawn_egg = register("dark_zombie_spawn_egg", new SpawnEggItem(NetherEntityTypes.DARK_ZOMBIE, 0x700c0c, 0x323131, new Item.Properties().group(NetherItemGroup.TOOLS)));
		bloody_zombie_spawn_egg = register("bloody_zombie_spawn_egg", new SpawnEggItem(NetherEntityTypes.BLOODY_ZOMBIE, 0x700c0c, 0x351212, new Item.Properties().group(NetherItemGroup.TOOLS)));
		lava_slime_spawn_egg = register("lava_slime_spawn_egg", new SpawnEggItem(NetherEntityTypes.LAVA_SLIME, 0xa43434, 0xbb5a29, new Item.Properties().group(NetherItemGroup.TOOLS)));
		camouflage_spider_spawn_egg = register("camouflage_spider_spawn_egg", new SpawnEggItem(NetherEntityTypes.CAMOUFLAGE_SPIDER, 0x3f172f, 0x710e20, new Item.Properties().group(NetherItemGroup.TOOLS)));
		tribal_warrior_spawn_egg = register("tribal_warrior_spawn_egg", new SpawnEggItem(NetherEntityTypes.TRIBAL_WARRIOR, 0x787878, 0x500e71, new Item.Properties().group(NetherItemGroup.TOOLS)));
		tribal_trainee_spawn_egg = register("tribal_trainee_spawn_egg", new SpawnEggItem(NetherEntityTypes.TRIBAL_TRAINEE, 0x787878, 0x710e20, new Item.Properties().group(NetherItemGroup.TOOLS)));
		tribal_archer_spawn_egg = register("tribal_archer_spawn_egg", new SpawnEggItem(NetherEntityTypes.TRIBAL_ARCHER, 0x787878, 0x7a5b89, new Item.Properties().group(NetherItemGroup.TOOLS)));

		glowood_stick = register("glowood_stick", new Item(new Item.Properties().group(NetherItemGroup.MISC)));
		foulite_dust = register("foulite_dust", new Item(new Item.Properties().group(NetherItemGroup.MISC)));
		imp_skin = register("imp_skin", new Item(new Item.Properties().group(NetherItemGroup.MISC)));
		w_dust = register("w_dust", new Item(new Item.Properties().group(NetherItemGroup.MISC)));
		w_obsidian_ingot = register("w_obsidian_ingot", new Item(new Item.Properties().group(NetherItemGroup.MISC)));
		neridium_ingot = register("neridium_ingot", new Item(new Item.Properties().group(NetherItemGroup.MISC)));
		pyridium_ingot = register("pyridium_ingot", new Item(new Item.Properties().group(NetherItemGroup.MISC)));
		linium_ingot = register("linium_ingot", new Item(new Item.Properties().group(NetherItemGroup.MISC)));
		red_feather = register("red_feather", new Item(new Item.Properties().group(NetherItemGroup.MISC)));
		lava_book = register("lava_book", new Item(new Item.Properties().group(NetherItemGroup.MISC)));
		lava_paper = register("lava_paper", new Item(new Item.Properties().group(NetherItemGroup.MISC)));
		ghast_rod = register("ghast_rod", new Item(new Item.Properties().group(NetherItemGroup.MISC)));
		ghast_bones = register("ghast_bones", new Item(new Item.Properties().group(NetherItemGroup.MISC)));
		ghast_marrow = register("ghast_marrow", new BoneMealItem(new Item.Properties().group(NetherItemGroup.MISC)));
		magma_caramel = register("magma_caramel", new Item(new Item.Properties().group(NetherItemGroup.MISC)));

		glowood_sword = register("glowood_sword", new SwordItem(NetherItemTier.GLOWWOOD, 3, -2.4F, (new Item.Properties()).group(NetherItemGroup.COMBAT)));
		glowood_pickaxe = register("glowood_pickaxe", new PickaxeItem(NetherItemTier.GLOWWOOD, 1, -2.8F, (new Item.Properties()).group(NetherItemGroup.TOOLS)));
		glowood_axe = register("glowood_axe", new AxeItem(NetherItemTier.GLOWWOOD, 6.0F, -3.2F, (new Item.Properties()).group(NetherItemGroup.TOOLS)));
		glowood_shovel = register("glowood_shovel", new ShovelItem(NetherItemTier.GLOWWOOD, 1.5F, -3.0F, (new Item.Properties()).group(NetherItemGroup.TOOLS)));
		glowood_hoe = register("glowood_hoe", new HoeItem(NetherItemTier.GLOWWOOD, -3.0F, (new Item.Properties()).group(NetherItemGroup.TOOLS)));

		netherrack_sword = register("netherrack_sword", new SwordItem(NetherItemTier.NETHERRACK, 3, -2.4F, (new Item.Properties()).group(NetherItemGroup.COMBAT)));
		netherrack_pickaxe = register("netherrack_pickaxe", new PickaxeItem(NetherItemTier.NETHERRACK, 1, -2.8F, (new Item.Properties()).group(NetherItemGroup.TOOLS)));
		netherrack_axe = register("netherrack_axe", new AxeItem(NetherItemTier.NETHERRACK, 7.0F, -3.2F, (new Item.Properties()).group(NetherItemGroup.TOOLS)));
		netherrack_shovel = register("netherrack_shovel", new ShovelItem(NetherItemTier.NETHERRACK, 1.5F, -3.0F, (new Item.Properties()).group(NetherItemGroup.TOOLS)));
		netherrack_hoe = register("netherrack_hoe", new HoeItem(NetherItemTier.NETHERRACK, -2.0F, (new Item.Properties()).group(NetherItemGroup.TOOLS)));

		neridium_sword = register("neridium_sword", new SwordItem(NetherItemTier.NERIDIUM, 3, -2.4F, (new Item.Properties()).group(NetherItemGroup.COMBAT)));
		neridium_pickaxe = register("neridium_pickaxe", new PickaxeItem(NetherItemTier.NERIDIUM, 1, -2.8F, (new Item.Properties()).group(NetherItemGroup.TOOLS)));
		neridium_axe = register("neridium_axe", new AxeItem(NetherItemTier.NERIDIUM, 6.0F, -3.1F, (new Item.Properties()).group(NetherItemGroup.TOOLS)));
		neridium_shovel = register("neridium_shovel", new ShovelItem(NetherItemTier.NERIDIUM, 1.5F, -3.0F, (new Item.Properties()).group(NetherItemGroup.TOOLS)));
		neridium_hoe = register("neridium_hoe", new HoeItem(NetherItemTier.NERIDIUM, -1.0F, (new Item.Properties()).group(NetherItemGroup.TOOLS)));

		pyridium_sword = register("pyridium_sword", new SwordItem(NetherItemTier.PYRIDIUM, 3, -2.4F, (new Item.Properties()).group(NetherItemGroup.COMBAT)));
		pyridium_pickaxe = register("pyridium_pickaxe", new PickaxeItem(NetherItemTier.PYRIDIUM, 1, -2.8F, (new Item.Properties()).group(NetherItemGroup.TOOLS)));
		pyridium_axe = register("pyridium_axe", new AxeItem(NetherItemTier.PYRIDIUM, 5.0F, -3.0F, (new Item.Properties()).group(NetherItemGroup.TOOLS)));
		pyridium_shovel = register("pyridium_shovel", new ShovelItem(NetherItemTier.PYRIDIUM, 1.5F, -3.0F, (new Item.Properties()).group(NetherItemGroup.TOOLS)));
		pyridium_hoe = register("pyridium_hoe", new HoeItem(NetherItemTier.PYRIDIUM, -0.0F, (new Item.Properties()).group(NetherItemGroup.TOOLS)));

		linium_sword = register("linium_sword", new SwordItem(NetherItemTier.LINIUM, 4, -2.4F, (new Item.Properties()).group(NetherItemGroup.COMBAT)));
		linium_pickaxe = register("linium_pickaxe", new PickaxeItem(NetherItemTier.LINIUM, 1, -2.8F, (new Item.Properties()).group(NetherItemGroup.TOOLS)));
		linium_axe = register("linium_axe", new AxeItem(NetherItemTier.LINIUM, 5.0F, -3.0F, (new Item.Properties()).group(NetherItemGroup.TOOLS)));
		linium_shovel = register("linium_shovel", new ShovelItem(NetherItemTier.LINIUM, 1.5F, -3.0F, (new Item.Properties()).group(NetherItemGroup.TOOLS)));
		linium_hoe = register("linium_hoe", new HoeItem(NetherItemTier.LINIUM, -0.0F, (new Item.Properties()).group(NetherItemGroup.TOOLS)));

		netherrack_bow = register("netherrack_bow", new NetherBowItem(new Item.Properties().defaultMaxDamage(256).group(NetherItemGroup.COMBAT)));
		neridium_bow = register("neridium_bow", new NetherBowItem(new Item.Properties().defaultMaxDamage(431).group(NetherItemGroup.COMBAT)));
		pyridium_bow = register("pyridium_bow", new NetherBowItem(new Item.Properties().defaultMaxDamage(816).group(NetherItemGroup.COMBAT)));
		linium_bow = register("linium_bow", new NetherBowItem(new Item.Properties().defaultMaxDamage(280).group(NetherItemGroup.COMBAT)));

		netherrack_arrow = register("netherrack_arrow", new NetherArrowItem(new Item.Properties().group(NetherItemGroup.COMBAT)));
		neridium_arrow = register("neridium_arrow", new NetherArrowItem(new Item.Properties().group(NetherItemGroup.COMBAT)));
		pyridium_arrow = register("pyridium_arrow", new NetherArrowItem(new Item.Properties().group(NetherItemGroup.COMBAT)));
		linium_arrow = register("linium_arrow", new NetherArrowItem(new Item.Properties().group(NetherItemGroup.COMBAT)));

		slime_eggs = register("slime_eggs", new SlimeEggItem(new Item.Properties().group(NetherItemGroup.MISC)));
		neridium_lighter = register("neridium_lighter", new NeridiumLighterItem(new Item.Properties().maxDamage(32).group(NetherItemGroup.TOOLS)));
		lava_boat = register("lava_boat", new LavaBoatItem(new Item.Properties().maxStackSize(1).group(NetherItemGroup.MISC)));
		nether_painting = register("nether_painting", new HangingEntityItem(EntityType.PAINTING, new Item.Properties().group(NetherItemGroup.MISC)));

		dark_seeds = register("dark_seeds", new BlockNamedItem(NetherBlocks.dark_wheat, new Item.Properties().group(NetherItemGroup.MISC)));
		dark_wheat = register("dark_wheat", new Item(new Item.Properties().group(NetherItemGroup.MISC)));
		devil_bread = register("devil_bread", new Item(new Item.Properties().food(NetherFoods.DEVIL_BREAD).group(NetherItemGroup.MISC)));
		glowood_bowl = register("glowood_bowl", new Item(new Item.Properties().group(NetherItemGroup.MISC)));
		glow_stew = register("glow_stew", new NetherSoupItem(new Item.Properties().food(NetherFoods.GLOW_STEW).group(NetherItemGroup.MISC).maxStackSize(1)));
		glow_apple = register("glow_apple", new Item(new Item.Properties().food(NetherFoods.GLOW_APPLE).group(NetherItemGroup.MISC)));

		imp_helmet = register("imp_helmet", new ArmorItem(NetherArmorMaterial.IMP_SKIN, EquipmentSlotType.HEAD, (new Item.Properties()).group(NetherItemGroup.COMBAT)));
		imp_chestplate = register("imp_chestplate", new ArmorItem(NetherArmorMaterial.IMP_SKIN, EquipmentSlotType.CHEST, (new Item.Properties()).group(NetherItemGroup.COMBAT)));
		imp_leggings = register("imp_leggings", new ArmorItem(NetherArmorMaterial.IMP_SKIN, EquipmentSlotType.LEGS, (new Item.Properties()).group(NetherItemGroup.COMBAT)));
		imp_boots = register("imp_boots", new ArmorItem(NetherArmorMaterial.IMP_SKIN, EquipmentSlotType.FEET, (new Item.Properties()).group(NetherItemGroup.COMBAT)));

		neridium_helmet = register("neridium_helmet", new ArmorItem(NetherArmorMaterial.NERIDIUM, EquipmentSlotType.HEAD, (new Item.Properties()).group(NetherItemGroup.COMBAT)));
		neridium_chestplate = register("neridium_chestplate", new ArmorItem(NetherArmorMaterial.NERIDIUM, EquipmentSlotType.CHEST, (new Item.Properties()).group(NetherItemGroup.COMBAT)));
		neridium_leggings = register("neridium_leggings", new ArmorItem(NetherArmorMaterial.NERIDIUM, EquipmentSlotType.LEGS, (new Item.Properties()).group(NetherItemGroup.COMBAT)));
		neridium_boots = register("neridium_boots", new ArmorItem(NetherArmorMaterial.NERIDIUM, EquipmentSlotType.FEET, (new Item.Properties()).group(NetherItemGroup.COMBAT)));

		w_obsidian_helmet = register("w_obsidian_helmet", new ArmorItem(NetherArmorMaterial.W_OBSIDIAN, EquipmentSlotType.HEAD, (new Item.Properties()).group(NetherItemGroup.COMBAT)));
		w_obsidian_chestplate = register("w_obsidian_chestplate", new ArmorItem(NetherArmorMaterial.W_OBSIDIAN, EquipmentSlotType.CHEST, (new Item.Properties()).group(NetherItemGroup.COMBAT)));
		w_obsidian_leggings = register("w_obsidian_leggings", new ArmorItem(NetherArmorMaterial.W_OBSIDIAN, EquipmentSlotType.LEGS, (new Item.Properties()).group(NetherItemGroup.COMBAT)));
		w_obsidian_boots = register("w_obsidian_boots", new ArmorItem(NetherArmorMaterial.W_OBSIDIAN, EquipmentSlotType.FEET, (new Item.Properties()).group(NetherItemGroup.COMBAT)));

		neridium_bucket = register("neridium_bucket", new NeridiumBucketItem(Fluids.EMPTY, new Item.Properties().maxStackSize(16).group(NetherItemGroup.TOOLS)));
		neridium_lava_bucket = register("neridium_lava_bucket", new NeridiumBucketItem(Fluids.LAVA, new Item.Properties().maxStackSize(1).containerItem(neridium_bucket).group(NetherItemGroup.TOOLS)));
	}

	private static void registerBlockItems()
	{
		for (int i3 = 0; i3 < NetherBlocks.blockList.size(); ++i3)
		{
			NethercraftRegistry.register(iItemRegistry, NetherBlocks.blockList.get(i3).getRegistryName().toString().replace("nethercraft:", ""), new BlockItem(NetherBlocks.blockList.get(i3), (new Item.Properties().group(NetherItemGroup.BLOCKS))));
		}

		lava_reeds = register("lava_reeds", new BlockItem(NetherBlocks.lava_reeds, (new Item.Properties()).group(NetherItemGroup.MISC)));

		foulite_torch = register("foulite_torch", new WallOrFloorItem(NetherBlocks.foulite_torch, NetherBlocks.foulite_wall_torch, (new Item.Properties()).group(NetherItemGroup.BLOCKS)));
		charcoal_torch = register("charcoal_torch", new WallOrFloorItem(NetherBlocks.charcoal_torch, NetherBlocks.charcoal_wall_torch, (new Item.Properties()).group(NetherItemGroup.BLOCKS)));
	}

	private static Item register(String name, Item item)
	{
		NethercraftRegistry.register(iItemRegistry, name, item);
		return item;
	}

}