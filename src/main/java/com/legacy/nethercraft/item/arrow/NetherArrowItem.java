package com.legacy.nethercraft.item.arrow;

import com.legacy.nethercraft.entity.projectile.LiniumArrowEntity;
import com.legacy.nethercraft.entity.projectile.NeridiumArrowEntity;
import com.legacy.nethercraft.entity.projectile.NetherrackArrowEntity;
import com.legacy.nethercraft.entity.projectile.PyridiumArrowEntity;
import com.legacy.nethercraft.item.NetherItems;

import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.item.ArrowItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class NetherArrowItem extends ArrowItem
{

	public NetherArrowItem(Item.Properties builder)
	{
		super(builder);
	}

	public AbstractArrowEntity createArrow(World worldIn, ItemStack stack, LivingEntity shooter)
	{
		if (this.getItem() == NetherItems.neridium_arrow)
		{
			return new NeridiumArrowEntity(worldIn, shooter);
		}
		else if (this.getItem() == NetherItems.pyridium_arrow)
		{
			return new PyridiumArrowEntity(worldIn, shooter);
		}
		else if (this.getItem() == NetherItems.linium_arrow)
		{
			return new LiniumArrowEntity(worldIn, shooter);
		}
		else
		{
			return new NetherrackArrowEntity(worldIn, shooter);
		}
	}

	@Override
	public boolean isInfinite(ItemStack stack, ItemStack bow, PlayerEntity player)
	{
		if (this.getItem() == NetherItems.pyridium_arrow || this.getItem() == NetherItems.linium_arrow)
		{
			return false;
		}
		else
		{
			int enchant = EnchantmentHelper.getEnchantmentLevel(Enchantments.INFINITY, bow);
			return enchant <= 0 ? false : this.getClass() == NetherArrowItem.class;
		}
	}
}