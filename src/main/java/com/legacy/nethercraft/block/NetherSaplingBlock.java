package com.legacy.nethercraft.block;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.SaplingBlock;
import net.minecraft.block.trees.Tree;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.server.ServerWorld;

public class NetherSaplingBlock extends SaplingBlock
{

	public NetherSaplingBlock(Tree treeIn, Properties properties)
	{
		super(treeIn, properties);
	}

	@Override
	public void tick(BlockState state, ServerWorld worldIn, BlockPos pos, Random random)
	{
		super.tick(state, worldIn, pos, random);
		if (!worldIn.isAreaLoaded(pos, 1))
			return;
		if (random.nextInt(7) == 0)
		{
			// grow
			this.func_226942_a_(worldIn, pos, state, random);
		}
	}

	@Override
	protected boolean isValidGround(BlockState state, IBlockReader worldIn, BlockPos pos)
	{
		Block block = state.getBlock();
		return block == Blocks.NETHERRACK || block == NetherBlocks.nether_dirt || super.isValidGround(state, worldIn, pos);
	}
}
