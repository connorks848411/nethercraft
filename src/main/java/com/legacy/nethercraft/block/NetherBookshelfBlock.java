package com.legacy.nethercraft.block;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorldReader;
import net.minecraftforge.common.extensions.IForgeBlock;

public class NetherBookshelfBlock extends Block implements IForgeBlock
{
	public NetherBookshelfBlock(Block.Properties props)
	{
		super(props);
	}

	@Override
	public float getEnchantPowerBonus(BlockState state, IWorldReader world, BlockPos pos)
	{
		return 1.0F;
	}
}
