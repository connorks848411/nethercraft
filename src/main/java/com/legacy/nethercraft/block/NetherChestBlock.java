package com.legacy.nethercraft.block;

import com.legacy.nethercraft.tile_entity.NetherChestTileEntity.GlowoodChestTileEntity;
import com.legacy.nethercraft.tile_entity.NetherTileEntityTypes;

import net.minecraft.block.ChestBlock;
import net.minecraft.tileentity.ChestTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockReader;

public class NetherChestBlock extends ChestBlock
{
	public NetherChestBlock(Properties properties)
	{
		super(properties, () ->
		{
			return NetherTileEntityTypes.GLOWOOD_CHEST;
		});
	}

	@Override
	public TileEntity createNewTileEntity(IBlockReader worldIn)
	{
		if (this == NetherBlocks.glowood_chest)
			return new GlowoodChestTileEntity();
		else
			return new ChestTileEntity();
	}
}
