package com.legacy.nethercraft.block.container;

import com.legacy.nethercraft.block.NetherBlocks;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.WorkbenchContainer;
import net.minecraft.util.IWorldPosCallable;

public class GlowoodWorkbenchContainer extends WorkbenchContainer
{
	private final IWorldPosCallable field_217070_e;

	public GlowoodWorkbenchContainer(int p_i50089_1_, PlayerInventory p_i50089_2_)
	{
		this(p_i50089_1_, p_i50089_2_, IWorldPosCallable.DUMMY);
	}

	public GlowoodWorkbenchContainer(int p_i50090_1_, PlayerInventory p_i50090_2_, IWorldPosCallable p_i50090_3_)
	{
		super(p_i50090_1_, p_i50090_2_, p_i50090_3_);
		this.field_217070_e = p_i50090_3_;
	}

	public boolean canInteractWith(PlayerEntity playerIn)
	{
		return isWithinUsableDistance(this.field_217070_e, playerIn, NetherBlocks.glowood_crafting_table);
	}
}