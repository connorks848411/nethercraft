package com.legacy.nethercraft.world;

import java.util.Random;

import com.legacy.nethercraft.block.NetherBlocks;
import com.legacy.nethercraft.entity.NetherEntityTypes;
import com.legacy.nethercraft.entity.goal.PigmanRangedBowAttackGoal;
import com.legacy.nethercraft.entity.tribal.TribalEntity;
import com.legacy.nethercraft.item.NetherItems;
import com.legacy.nethercraft.world.feature.GlowoodTreeFeature;
import com.legacy.nethercraft.world.feature.LavaReedFeature;

import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.goal.ZombieAttackGoal;
import net.minecraft.entity.monster.GhastEntity;
import net.minecraft.entity.monster.ZombiePigmanEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.BowItem;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IndirectEntityDamageSource;
import net.minecraft.world.GameRules;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.Biomes;
import net.minecraft.world.gen.GenerationStage;
import net.minecraft.world.gen.blockplacer.SimpleBlockPlacer;
import net.minecraft.world.gen.blockstateprovider.SimpleBlockStateProvider;
import net.minecraft.world.gen.feature.BlockClusterFeatureConfig;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.IFeatureConfig;
import net.minecraft.world.gen.feature.NoFeatureConfig;
import net.minecraft.world.gen.feature.OreFeatureConfig;
import net.minecraft.world.gen.feature.OreFeatureConfig.FillerBlockType;
import net.minecraft.world.gen.feature.TreeFeatureConfig;
import net.minecraft.world.gen.foliageplacer.BlobFoliagePlacer;
import net.minecraft.world.gen.placement.ChanceRangeConfig;
import net.minecraft.world.gen.placement.CountRangeConfig;
import net.minecraft.world.gen.placement.FrequencyConfig;
import net.minecraft.world.gen.placement.Placement;
import net.minecraft.world.gen.surfacebuilders.ConfiguredSurfaceBuilder;
import net.minecraft.world.gen.surfacebuilders.SurfaceBuilder;
import net.minecraft.world.gen.surfacebuilders.SurfaceBuilderConfig;
import net.minecraftforge.common.BiomeDictionary;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.living.LivingAttackEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.event.entity.living.LivingSetAttackTargetEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.registries.ForgeRegistries;

public class NetherEvents
{
	public static final TreeFeatureConfig GLOWOOD_CONFIG = (new TreeFeatureConfig.Builder(new SimpleBlockStateProvider(NetherBlocks.glowood_log.getDefaultState()), new SimpleBlockStateProvider(NetherBlocks.glowood_leaves.getDefaultState()), new BlobFoliagePlacer(2, 0))).baseHeight(5).heightRandA(2).foliageHeight(3).ignoreVines().setSapling((net.minecraftforge.common.IPlantable) NetherBlocks.glowood_sapling).build();
	public static final BlockClusterFeatureConfig PURPLE_MUSHROOM_CONFIG = (new BlockClusterFeatureConfig.Builder(new SimpleBlockStateProvider(NetherBlocks.purple_glowshroom.getDefaultState()), new SimpleBlockPlacer())).tries(64).func_227317_b_().build();
	public static final BlockClusterFeatureConfig GREEN_MUSHROOM_CONFIG = (new BlockClusterFeatureConfig.Builder(new SimpleBlockStateProvider(NetherBlocks.green_glowshroom.getDefaultState()), new SimpleBlockPlacer())).tries(64).func_227317_b_().build();

	private Random rand;

	public NetherEvents()
	{
		this.rand = new Random();
	}

	public static void setupNetherFeatures(FMLCommonSetupEvent event)
	{
		for (Biome biome : ForgeRegistries.BIOMES.getValues())
		{
			if (BiomeDictionary.hasType(biome, BiomeDictionary.Type.NETHER))
			{
				// Literally everything from mob spawning, and biome decoration
				biome.addFeature(GenerationStage.Decoration.UNDERGROUND_ORES, Feature.ORE.withConfiguration(new OreFeatureConfig(FillerBlockType.NETHERRACK, NetherBlocks.foulite_ore.getDefaultState(), 14)).withPlacement(Placement.COUNT_RANGE.configure(new CountRangeConfig(20, 10, 20, 128))));
				biome.addFeature(GenerationStage.Decoration.UNDERGROUND_ORES, Feature.ORE.withConfiguration(new OreFeatureConfig(FillerBlockType.NETHERRACK, NetherBlocks.neridium_ore.getDefaultState(), 8)).withPlacement(Placement.COUNT_RANGE.configure(new CountRangeConfig(8, 10, 20, 128))));
				biome.addFeature(GenerationStage.Decoration.UNDERGROUND_ORES, Feature.ORE.withConfiguration(new OreFeatureConfig(FillerBlockType.NETHERRACK, NetherBlocks.pyridium_ore.getDefaultState(), 5)).withPlacement(Placement.COUNT_RANGE.configure(new CountRangeConfig(4, 10, 20, 128))));
				biome.addFeature(GenerationStage.Decoration.UNDERGROUND_ORES, Feature.ORE.withConfiguration(new OreFeatureConfig(FillerBlockType.NETHERRACK, NetherBlocks.linium_ore.getDefaultState(), 3)).withPlacement(Placement.COUNT_RANGE.configure(new CountRangeConfig(5, 10, 20, 128))));
				biome.addFeature(GenerationStage.Decoration.UNDERGROUND_ORES, Feature.ORE.withConfiguration(new OreFeatureConfig(FillerBlockType.NETHERRACK, NetherBlocks.w_ore.getDefaultState(), 4)).withPlacement(Placement.COUNT_RANGE.configure(new CountRangeConfig(2, 10, 20, 128))));

				biome.getSpawns(EntityClassification.CREATURE).add(new Biome.SpawnListEntry(NetherEntityTypes.IMP, 30, 2, 3));

				biome.getSpawns(EntityClassification.MONSTER).add(new Biome.SpawnListEntry(NetherEntityTypes.DARK_ZOMBIE, 50, 0, 2));
				biome.getSpawns(EntityClassification.MONSTER).add(new Biome.SpawnListEntry(NetherEntityTypes.BLOODY_ZOMBIE, 50, 0, 2));
				biome.getSpawns(EntityClassification.MONSTER).add(new Biome.SpawnListEntry(NetherEntityTypes.CAMOUFLAGE_SPIDER, 60, 1, 3));
				biome.getSpawns(EntityClassification.MONSTER).add(new Biome.SpawnListEntry(NetherEntityTypes.LAVA_SLIME, 40, 1, 2));

				biome.getSpawns(EntityClassification.MONSTER).add(new Biome.SpawnListEntry(NetherEntityTypes.TRIBAL_WARRIOR, 50, 0, 3));
				biome.getSpawns(EntityClassification.MONSTER).add(new Biome.SpawnListEntry(NetherEntityTypes.TRIBAL_ARCHER, 50, 1, 2));
				biome.getSpawns(EntityClassification.MONSTER).add(new Biome.SpawnListEntry(NetherEntityTypes.TRIBAL_TRAINEE, 50, 1, 3));

				if (biome == Biomes.NETHER)
				{
					biome.addFeature(GenerationStage.Decoration.VEGETAL_DECORATION, new GlowoodTreeFeature(TreeFeatureConfig::func_227338_a_, true).withConfiguration(GLOWOOD_CONFIG).withPlacement(Placement.COUNT_RANGE.configure(new CountRangeConfig(40, 30, 0, 50))));

					biome.addFeature(GenerationStage.Decoration.VEGETAL_DECORATION, new LavaReedFeature(NoFeatureConfig::deserialize).withConfiguration(IFeatureConfig.NO_FEATURE_CONFIG).withPlacement(Placement.COUNT_HEIGHTMAP_DOUBLE.configure(new FrequencyConfig(20))));

					biome.addFeature(GenerationStage.Decoration.UNDERGROUND_DECORATION, Feature.RANDOM_PATCH.withConfiguration(PURPLE_MUSHROOM_CONFIG).withPlacement(Placement.CHANCE_RANGE.configure(new ChanceRangeConfig(0.5F, 0, 0, 128))));
					biome.addFeature(GenerationStage.Decoration.UNDERGROUND_DECORATION, Feature.RANDOM_PATCH.withConfiguration(GREEN_MUSHROOM_CONFIG).withPlacement(Placement.CHANCE_RANGE.configure(new ChanceRangeConfig(0.5F, 0, 0, 128))));

					NetherEvents.setNetherBuilder(biome, new ConfiguredSurfaceBuilder<>(new NetherDirtSurfaceBuilder(SurfaceBuilderConfig::deserialize), SurfaceBuilder.NETHERRACK_CONFIG));
				}
			}
		}
	}

	@SubscribeEvent
	public void livingSpawn(EntityJoinWorldEvent event)
	{
		if (event.getEntity() instanceof ZombiePigmanEntity)
		{
			if (this.rand.nextInt(5) == 0 && !((ZombiePigmanEntity) event.getEntity()).isChild())
			{
				event.getEntity().setItemStackToSlot(EquipmentSlotType.MAINHAND, new ItemStack(NetherItems.pyridium_bow));
				((ZombiePigmanEntity) event.getEntity()).goalSelector.addGoal(1, new PigmanRangedBowAttackGoal<ZombiePigmanEntity>((ZombiePigmanEntity) event.getEntity(), 1.0D, 20, 15.0F));
				((ZombiePigmanEntity) event.getEntity()).getAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(30.0F);
				((ZombiePigmanEntity) event.getEntity()).setHealth(30.0F);
			}
		}
	}

	@SubscribeEvent
	public void onEntityUpdate(LivingUpdateEvent event)
	{
		if (event.getEntityLiving() instanceof ZombiePigmanEntity)
		{
			ZombiePigmanEntity pigman = (ZombiePigmanEntity) event.getEntityLiving();
			boolean removedMelee = false;

			if (pigman.getHeldItemMainhand().getItem() instanceof BowItem && !removedMelee)
			{
				pigman.goalSelector.removeGoal(new ZombieAttackGoal((ZombiePigmanEntity) event.getEntity(), 1.0D, false));
				removedMelee = true;
			}

			if (pigman.getAttackTarget() != null && pigman.getAttackTarget() instanceof TribalEntity && (pigman.getAttackTarget().getHealth() <= 0 || !pigman.getAttackTarget().isAlive()))
			{
				this.removePigmanAnger(pigman);
			}
		}
	}

	@SubscribeEvent
	public void onEntityDeath(LivingDeathEvent event)
	{
		if (event.getEntityLiving() instanceof GhastEntity && event.getEntityLiving().world.rand.nextInt(3) == 0 && event.getEntityLiving().world.getGameRules().getBoolean(GameRules.DO_ENTITY_DROPS))
		{
			ItemStack bones = new ItemStack(NetherItems.ghast_bones);
			bones.setCount(event.getEntityLiving().world.rand.nextInt(2) + 1);
			event.getEntityLiving().entityDropItem(bones);
		}
	}

	@SubscribeEvent
	public void onEntityAttack(LivingAttackEvent event)
	{
		if (event.getEntityLiving() instanceof ZombiePigmanEntity)
		{
			if (event.getSource() instanceof IndirectEntityDamageSource && event.getSource().getTrueSource() instanceof ZombiePigmanEntity)
			{
				event.getSource().getImmediateSource().remove();
				event.setCanceled(true);
			}
		}
	}

	@SubscribeEvent
	public void onAttackTarget(LivingSetAttackTargetEvent event)
	{
		if (event.getEntityLiving() instanceof ZombiePigmanEntity && event.getTarget() instanceof TribalEntity && !event.getEntityLiving().canEntityBeSeen(event.getTarget()))
		{
			this.removePigmanAnger((ZombiePigmanEntity) event.getEntityLiving());
		}
	}

	/*@SubscribeEvent
	public void onZombieSummoned(SummonAidEvent event)
	{
		if (event.getEntity() instanceof DarkZombieEntity)
		{
			event.setCustomSummonedAid(NetherEntityTypes.DARK_ZOMBIE.create(event.getWorld()));
		}
	
		if (event.getEntity() instanceof BloodyZombieEntity)
		{
			event.setCustomSummonedAid(NetherEntityTypes.BLOODY_ZOMBIE.create(event.getWorld()));
		}
	}*/

	/*@SubscribeEvent
	public void onPlayerBreakSpeed(PlayerEvent.BreakSpeed event)
	{
		if (event.getPlayer().getHeldItemMainhand().getItem() == NetherItems.linium_pickaxe && event.getState().getBlock() == Blocks.NETHERRACK)
		{
			event.setNewSpeed(event.getOriginalSpeed() - 2.0F);
		}
	}*/

	private void removePigmanAnger(ZombiePigmanEntity pigman)
	{
		// TODO Fix pigmen getting angry at players after a tribal hits them. This is an
		// awful issue, and unfortunately this fix here can cause random crashes.
		/*ObfuscationReflectionHelper.setPrivateValue(ZombiePigmanEntity.class, pigman, 0, "field_70837_d");
		pigman.setRevengeTarget(null);
		pigman.setAttackTarget(null);*/
	}

	public static void setNetherBuilder(Biome biome, ConfiguredSurfaceBuilder<?> builder)
	{
		try
		{
			ObfuscationReflectionHelper.setPrivateValue(Biome.class, biome, builder, "field_201875_ar");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}