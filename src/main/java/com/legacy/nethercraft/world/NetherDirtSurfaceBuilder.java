package com.legacy.nethercraft.world;

import java.util.Random;
import java.util.function.Function;

import com.legacy.nethercraft.block.NetherBlocks;
import com.mojang.datafixers.Dynamic;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.util.SharedSeedRandom;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.chunk.IChunk;
import net.minecraft.world.gen.OctavesNoiseGenerator;
import net.minecraft.world.gen.surfacebuilders.SurfaceBuilder;
import net.minecraft.world.gen.surfacebuilders.SurfaceBuilderConfig;

public class NetherDirtSurfaceBuilder extends SurfaceBuilder<SurfaceBuilderConfig>
{

	private static final BlockState CAVE_AIR = Blocks.CAVE_AIR.getDefaultState();

	private static final BlockState NETHERRACK = Blocks.NETHERRACK.getDefaultState();

	private static final BlockState GRAVEL = Blocks.GRAVEL.getDefaultState();

	private static final BlockState SOUL_SAND = Blocks.SOUL_SAND.getDefaultState();

	private static final BlockState NETHER_DIRT = NetherBlocks.nether_dirt.getDefaultState();
	
	private static final BlockState HEAT_SAND = NetherBlocks.heat_sand.getDefaultState();

	protected long field_205552_a;

	protected OctavesNoiseGenerator field_205553_b;

	public NetherDirtSurfaceBuilder(Function<Dynamic<?>, ? extends SurfaceBuilderConfig> p_i51308_1_)
	{
		super(p_i51308_1_);
	}

	@SuppressWarnings("deprecation")
	@Override
	public void buildSurface(Random random, IChunk chunkIn, Biome biomeIn, int x, int z, int startHeight, double noise, BlockState defaultBlock, BlockState defaultFluid, int seaLevel, long seed, SurfaceBuilderConfig config)
	{
		int i = seaLevel + 1;
		int j = x & 15;
		int k = z & 15;
		boolean flag = this.field_205553_b.func_205563_a((double) x * 0.03125D, (double) z * 0.03125D, 0.0D) + random.nextDouble() * 0.2D > 0.0D;
		boolean flag1 = this.field_205553_b.func_205563_a((double) x * 0.03125D, 109.0D, (double) z * 0.03125D) + random.nextDouble() * 0.2D > 0.0D;
		int l = (int) (noise / 3.0D + 3.0D + random.nextDouble() * 0.25D);
		BlockPos.Mutable blockpos$mutableblockpos = new BlockPos.Mutable();
		int i1 = -1;
		BlockState blockstate = NETHER_DIRT;
		BlockState blockstate1 = NETHER_DIRT;
		for (int j1 = 127; j1 >= 0; --j1)
		{
			blockpos$mutableblockpos.setPos(j, j1, k);
			BlockState blockstate2 = chunkIn.getBlockState(blockpos$mutableblockpos);
			if (blockstate2.getBlock() != null && !blockstate2.isAir())
			{	
				if (blockstate2.getBlock() == Blocks.NETHERRACK || blockstate2.getBlock() == NetherBlocks.nether_dirt)
				{
					if (i1 == -1)
					{
						if (l <= 0)
						{
							blockstate = CAVE_AIR;
							blockstate1 = NETHER_DIRT;
						}
						else if (j1 > 80 && j1 < 110)
						{
							blockstate = NETHERRACK;
							blockstate1 = NETHER_DIRT;
						}
						else if (j1 >= i - 4 && j1 <= i + 1)
						{
							blockstate = NETHER_DIRT;
							blockstate1 = NETHER_DIRT;

							if (flag1)
							{
								blockstate = SOUL_SAND;
								blockstate1 = HEAT_SAND;
							}
							if (flag)
							{
								blockstate = HEAT_SAND;
								blockstate1 = GRAVEL;
							}
						}
						if (j1 < i && (blockstate == null || blockstate.isAir()))
						{
							blockstate = Blocks.LAVA.getDefaultState();
						}
						i1 = l;
						if (j1 >= i - 1)
						{
							chunkIn.setBlockState(blockpos$mutableblockpos, blockstate, false);
						}
						else
						{
							chunkIn.setBlockState(blockpos$mutableblockpos, blockstate1, false);
						}
					}
					else if (i1 > 0)
					{
						--i1;
						chunkIn.setBlockState(blockpos$mutableblockpos, blockstate1, false);
					}
				}
			}
			else
			{
				i1 = -1;
			}
		}
	}

	public void setSeed(long seed)
	{
		if (this.field_205552_a != seed || this.field_205553_b == null)
		{
			this.field_205553_b = new OctavesNoiseGenerator(new SharedSeedRandom(seed), 4, 0);
		}
		this.field_205552_a = seed;
	}
}