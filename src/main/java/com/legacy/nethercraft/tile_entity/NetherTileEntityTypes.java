package com.legacy.nethercraft.tile_entity;

import com.legacy.nethercraft.Nethercraft;
import com.legacy.nethercraft.NethercraftRegistry;
import com.legacy.nethercraft.block.NetherBlocks;
import com.legacy.nethercraft.tile_entity.NetherChestTileEntity.GlowoodChestTileEntity;

import net.minecraft.tileentity.TileEntityType;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.registries.ObjectHolder;

@ObjectHolder(Nethercraft.MODID)
public class NetherTileEntityTypes
{

	public static final TileEntityType<NetherrackFurnaceTileEntity> NETHERRACK_FURNACE = null;

	public static final TileEntityType<GlowoodChestTileEntity> GLOWOOD_CHEST = null;

	public static void init(Register<TileEntityType<?>> event)
	{
		NethercraftRegistry.register(event.getRegistry(), "netherrack_furnace", TileEntityType.Builder.create(NetherrackFurnaceTileEntity::new, NetherBlocks.netherrack_furnace).build(null));
		NethercraftRegistry.register(event.getRegistry(), "glowood_chest", TileEntityType.Builder.create(GlowoodChestTileEntity::new, NetherBlocks.glowood_chest).build(null));
	}
}
