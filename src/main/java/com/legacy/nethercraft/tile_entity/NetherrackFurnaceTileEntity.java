package com.legacy.nethercraft.tile_entity;

import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.FurnaceContainer;
import net.minecraft.item.crafting.IRecipeType;
import net.minecraft.tileentity.AbstractFurnaceTileEntity;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;

public class NetherrackFurnaceTileEntity extends AbstractFurnaceTileEntity
{

	public NetherrackFurnaceTileEntity()
	{
		super(NetherTileEntityTypes.NETHERRACK_FURNACE, IRecipeType.SMELTING);
	}

	protected ITextComponent getDefaultName()
	{
		return new TranslationTextComponent("block.nethercraft.netherrack_furnace");
	}

	protected Container createMenu(int id, PlayerInventory player)
	{
		return new FurnaceContainer(id, player, this, this.furnaceData);
	}
}