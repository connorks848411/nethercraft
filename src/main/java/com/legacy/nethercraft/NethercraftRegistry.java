package com.legacy.nethercraft;

import com.legacy.nethercraft.block.NetherBlocks;
import com.legacy.nethercraft.entity.NetherEntityTypes;
import com.legacy.nethercraft.item.NetherItems;
import com.legacy.nethercraft.tile_entity.NetherTileEntityTypes;

import net.minecraft.block.Block;
import net.minecraft.entity.EntityType;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntityType;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.IForgeRegistryEntry;

@EventBusSubscriber(modid = Nethercraft.MODID, bus = Bus.MOD)
public class NethercraftRegistry
{

	@SubscribeEvent
	public static void onRegisterBlocks(Register<Block> event)
	{
		NetherBlocks.initialization(event);
	}

	@SubscribeEvent
	public static void onRegisterItems(Register<Item> event)
	{
		NetherItems.initialization(event);
	}

	@SubscribeEvent
	public static void onRegisterEntityTypes(Register<EntityType<?>> event)
	{
		NetherEntityTypes.init(event);
	}

	@SubscribeEvent
	public static void registerTileEntityTypes(Register<TileEntityType<?>> event)
	{
		NetherTileEntityTypes.init(event);
	}

	/*@SubscribeEvent
	public static void registerFeatures(Register<Feature<?>> event)
	{
		NetherFeatures.init(event);
	}*/

	public static <T extends IForgeRegistryEntry<T>> void register(IForgeRegistry<T> registry, String name, T object)
	{
		object.setRegistryName(Nethercraft.locate(name));
		registry.register(object);
	}
}