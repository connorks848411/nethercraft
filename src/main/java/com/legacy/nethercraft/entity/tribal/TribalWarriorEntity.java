package com.legacy.nethercraft.entity.tribal;

import com.legacy.nethercraft.item.NetherItems;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.goal.MeleeAttackGoal;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.World;

public class TribalWarriorEntity extends TribalEntity
{

	public static final DataParameter<Integer> WARRIOR_TYPE = EntityDataManager.<Integer> createKey(TribalWarriorEntity.class, DataSerializers.VARINT);

	public TribalWarriorEntity(EntityType<? extends TribalWarriorEntity> type, World world)
	{
		super(type, world);
	}

	@Override
	protected void registerGoals()
	{
		super.registerGoals();

		this.goalSelector.addGoal(2, new MeleeAttackGoal(this, 1.0D, false));
	}

	protected void registerAttributes()
	{
		super.registerAttributes();

		this.getAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(30.0D);
		this.getAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).setBaseValue(5.0D);
	}

	@Override
	public void writeAdditional(CompoundNBT compound)
	{
		super.writeAdditional(compound);
		compound.putInt("WarriorType", this.getWarriorType());
	}

	@Override
	public void read(CompoundNBT compound)
	{
		super.read(compound);
		this.setWarriorType(compound.getInt("WarriorType"));
	}

	@Override
	protected void registerData()
	{
		super.registerData();
		this.dataManager.register(WARRIOR_TYPE, Integer.valueOf(this.rand.nextInt(5)));
	}

	protected void setEquipmentBasedOnDifficulty(DifficultyInstance difficulty)
	{
		this.setItemStackToSlot(EquipmentSlotType.MAINHAND, new ItemStack(NetherItems.neridium_sword));
	}

	public void setWarriorType(int type)
	{
		this.dataManager.set(WARRIOR_TYPE, type);
	}

	public int getWarriorType()
	{
		return this.dataManager.get(WARRIOR_TYPE);
	}
}
