package com.legacy.nethercraft.entity.goal;

import java.util.EnumSet;

import com.legacy.nethercraft.item.NetherItems;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.entity.monster.MonsterEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.entity.projectile.ProjectileHelper;
import net.minecraft.item.BowItem;
import net.minecraft.item.ItemStack;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.MathHelper;

public class PigmanRangedBowAttackGoal<T extends MonsterEntity> extends Goal
{

	private final T entity;

	private final double moveSpeedAmp;

	private int attackCooldown;

	private final float maxAttackDistance;

	private int attackTime = -1;

	private int seeTime;

	private boolean strafingClockwise;

	private boolean strafingBackwards;

	private int strafingTime = -1;

	public PigmanRangedBowAttackGoal(T mob, double moveSpeedAmpIn, int attackCooldownIn, float maxAttackDistanceIn)
	{
		this.entity = mob;
		this.moveSpeedAmp = moveSpeedAmpIn;
		this.attackCooldown = attackCooldownIn;
		this.maxAttackDistance = maxAttackDistanceIn * maxAttackDistanceIn;
		this.setMutexFlags(EnumSet.of(Goal.Flag.MOVE, Goal.Flag.LOOK));
	}

	public void setAttackCooldown(int p_189428_1_)
	{
		this.attackCooldown = p_189428_1_;
	}

	@Override
	public boolean shouldExecute()
	{
		return this.entity.getAttackTarget() == null ? false : this.isBowInMainhand();
	}

	protected boolean isBowInMainhand()
	{
		ItemStack main = this.entity.getHeldItemMainhand();
		ItemStack off = this.entity.getHeldItemOffhand();
		return main.getItem() instanceof BowItem || off.getItem() instanceof BowItem;
	}

	@Override
	public boolean shouldContinueExecuting()
	{
		return (this.shouldExecute() || !this.entity.getNavigator().noPath()) && this.isBowInMainhand();
	}

	@Override
	public void startExecuting()
	{
		super.startExecuting();
		this.entity.setAggroed(true);
	}

	@Override
	public void resetTask()
	{
		super.resetTask();
		this.entity.setAggroed(false);
		this.seeTime = 0;
		this.attackTime = -1;
		this.entity.resetActiveHand();
	}

	@Override
	public void tick()
	{
		LivingEntity livingentity = this.entity.getAttackTarget();
		if (livingentity != null)
		{
			double d0 = this.entity.getDistanceSq(livingentity.getPosition().getX(), livingentity.getBoundingBox().minY, livingentity.getPosition().getZ());
			boolean flag = this.entity.getEntitySenses().canSee(livingentity);
			boolean flag1 = this.seeTime > 0;
			if (flag != flag1)
			{
				this.seeTime = 0;
			}
			if (flag)
			{
				++this.seeTime;
			}
			else
			{
				--this.seeTime;
			}
			if (!(d0 > (double) this.maxAttackDistance) && this.seeTime >= 20)
			{
				this.entity.getNavigator().clearPath();
				++this.strafingTime;
			}
			else
			{
				this.entity.getNavigator().tryMoveToEntityLiving(livingentity, this.moveSpeedAmp);
				this.strafingTime = -1;
			}
			if (this.strafingTime >= 20)
			{
				if ((double) this.entity.getRNG().nextFloat() < 0.3D)
				{
					this.strafingClockwise = !this.strafingClockwise;
				}
				if ((double) this.entity.getRNG().nextFloat() < 0.3D)
				{
					this.strafingBackwards = !this.strafingBackwards;
				}
				this.strafingTime = 0;
			}
			if (this.strafingTime > -1)
			{
				if (d0 > (double) (this.maxAttackDistance * 0.75F))
				{
					this.strafingBackwards = false;
				}
				else if (d0 < (double) (this.maxAttackDistance * 0.25F))
				{
					this.strafingBackwards = true;
				}
				this.entity.getMoveHelper().strafe(this.strafingBackwards ? -0.5F : 0.5F, this.strafingClockwise ? 0.5F : -0.5F);
				this.entity.faceEntity(livingentity, 30.0F, 30.0F);
			}
			else
			{
				this.entity.getLookController().setLookPositionWithEntity(livingentity, 30.0F, 30.0F);
			}
			if (this.entity.isHandActive())
			{
				if (!flag && this.seeTime < -60)
				{
					this.entity.resetActiveHand();
				}
				else if (flag)
				{
					int i = this.entity.getItemInUseMaxCount();
					if (i >= 20)
					{
						this.entity.resetActiveHand();
						this.attackEntityWithRangedAttack(livingentity, BowItem.getArrowVelocity(i));
						this.attackTime = this.attackCooldown;
					}
				}
			}
			else if (--this.attackTime <= 0 && this.seeTime >= -60)
			{
				this.entity.setActiveHand(ProjectileHelper.getHandWith(this.entity, NetherItems.pyridium_bow));
			}
		}
	}

	public void attackEntityWithRangedAttack(LivingEntity target, float distanceFactor)
	{
		ItemStack itemstack = new ItemStack(NetherItems.netherrack_arrow);
		AbstractArrowEntity abstractarrowentity = ProjectileHelper.fireArrow(this.entity, itemstack, distanceFactor);
		if (this.entity.getHeldItemMainhand().getItem() instanceof BowItem)
			abstractarrowentity = ((BowItem) this.entity.getHeldItemMainhand().getItem()).customeArrow(abstractarrowentity);
		double d0 = target.getPosition().getX() - this.entity.getPosition().getX();
		double d1 = target.getBoundingBox().minY + (double) (target.getHeight() / 3.0F) - abstractarrowentity.getPosition().getY();
		double d2 = target.getPosition().getZ() - this.entity.getPosition().getZ();
		double d3 = (double) MathHelper.sqrt(d0 * d0 + d2 * d2);
		abstractarrowentity.shoot(d0, d1 + d3 * (double) 0.2F, d2, 1.6F, (float) (14 - this.entity.world.getDifficulty().getId() * 4));
		this.entity.playSound(SoundEvents.ENTITY_ARROW_SHOOT, 1.0F, 1.0F / (this.entity.getRNG().nextFloat() * 0.4F + 0.8F));
		this.entity.world.addEntity(abstractarrowentity);
	}
}