package com.legacy.nethercraft.entity.misc;

import java.util.Iterator;
import java.util.List;

import javax.annotation.Nullable;

import com.google.common.collect.Lists;
import com.legacy.nethercraft.entity.NetherEntityTypes;
import com.legacy.nethercraft.item.NetherItems;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.item.PaintingEntity;
import net.minecraft.entity.item.PaintingType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.IPacket;
import net.minecraft.util.Direction;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.GameRules;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.FMLPlayMessages;
import net.minecraftforge.fml.network.NetworkHooks;

public class NetherPaintingEntity extends PaintingEntity
{

	public NetherPaintingEntity(EntityType<? extends NetherPaintingEntity> type, World world)
	{
		super(type, world);
	}

	@SuppressWarnings("deprecation")
	public NetherPaintingEntity(World worldIn, BlockPos pos, Direction facing)
	{
		super(worldIn, pos, facing);
		List<PaintingType> list = Lists.newArrayList();
		int i = 0;
		for (PaintingType paintingtype : Registry.MOTIVE)
		{
			this.art = paintingtype;
			this.updateFacingWithBoundingBox(facing);
			if (this.onValidSurface())
			{
				list.add(paintingtype);
				int j = paintingtype.getWidth() * paintingtype.getHeight();
				if (j > i)
				{
					i = j;
				}
			}
		}
		if (!list.isEmpty())
		{
			Iterator<PaintingType> iterator = list.iterator();
			while (iterator.hasNext())
			{
				PaintingType paintingtype1 = iterator.next();
				if (paintingtype1.getWidth() * paintingtype1.getHeight() < i)
				{
					iterator.remove();
				}
			}
			this.art = list.get(this.rand.nextInt(list.size()));
		}
		this.updateFacingWithBoundingBox(facing);
	}

	public NetherPaintingEntity(FMLPlayMessages.SpawnEntity spawnEntity, World world)
	{
		this(NetherEntityTypes.NETHER_PAINTING, world);
	}

	@Override
	public void onBroken(@Nullable Entity brokenEntity)
	{
		if (this.world.getGameRules().getBoolean(GameRules.DO_ENTITY_DROPS))
		{
			this.playSound(SoundEvents.ENTITY_PAINTING_BREAK, 1.0F, 1.0F);
			if (brokenEntity instanceof PlayerEntity)
			{
				PlayerEntity playerentity = (PlayerEntity) brokenEntity;
				if (playerentity.abilities.isCreativeMode)
				{
					return;
				}
			}
			this.entityDropItem(NetherItems.nether_painting);
		}
	}

	@Override
	public IPacket<?> createSpawnPacket()
	{
		return NetworkHooks.getEntitySpawningPacket(this);
	}
}
