package com.legacy.nethercraft.entity.misc;

import javax.annotation.Nullable;

import com.legacy.nethercraft.entity.NetherEntityTypes;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntitySize;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.MoverType;
import net.minecraft.entity.Pose;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.world.Explosion;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.FMLPlayMessages;
import net.minecraftforge.fml.network.NetworkHooks;

public class GhastBombEntity extends Entity
{

	private static final DataParameter<Integer> FUSE = EntityDataManager.createKey(GhastBombEntity.class, DataSerializers.VARINT);

	@Nullable
	private LivingEntity tntPlacedBy;

	private int fuse = 180;

	public GhastBombEntity(EntityType<? extends GhastBombEntity> p_i50216_1_, World p_i50216_2_)
	{
		super(p_i50216_1_, p_i50216_2_);
	}

	public GhastBombEntity(World worldIn, double x, double y, double z, @Nullable LivingEntity igniter)
	{
		this(NetherEntityTypes.GHAST_BOMB, worldIn);
		this.setPosition(x, y, z);
		double d0 = worldIn.rand.nextDouble() * (double) ((float) Math.PI * 2F);
		this.setMotion(-Math.sin(d0) * 0.02D, (double) 0.2F, -Math.cos(d0) * 0.02D);
		this.setFuse(180);
		this.prevPosX = x;
		this.prevPosY = y;
		this.prevPosZ = z;
		this.tntPlacedBy = igniter;
	}
	
	public GhastBombEntity(FMLPlayMessages.SpawnEntity spawnEntity, World world)
	{
		this(NetherEntityTypes.GHAST_BOMB, world);
	}

	protected void registerData()
	{
		this.dataManager.register(FUSE, 180);
	}

	protected boolean canTriggerWalking()
	{
		return false;
	}

	public boolean canBeCollidedWith()
	{
		return this.isAlive();
	}

	public void tick()
	{
		this.prevPosX = this.getPosition().getX();
		this.prevPosY = this.getPosition().getY();
		this.prevPosZ = this.getPosition().getZ();
		if (!this.hasNoGravity())
		{
			this.setMotion(this.getMotion().add(0.0D, -0.04D, 0.0D));
		}
		this.move(MoverType.SELF, this.getMotion());
		this.setMotion(this.getMotion().scale(0.98D));
		if (this.onGround)
		{
			this.setMotion(this.getMotion().mul(0.7D, -0.5D, 0.7D));
		}
		--this.fuse;
		if (this.fuse <= 0)
		{
			this.remove();
			if (!this.world.isRemote)
			{
				this.explode();
			}
		}
		else
		{
			this.handleWaterMovement();
			this.world.addParticle(ParticleTypes.SMOKE, this.getPosition().getX(), this.getPosition().getY() + 0.5D, this.getPosition().getZ(), 0.0D, 0.0D, 0.0D);
		}
	}

	private void explode()
	{
		float f = 8.0F;
		this.world.createExplosion(this, this.getPosition().getX(), this.getPosition().getY() + (double) (this.getHeight() / 16.0F), this.getPosition().getZ(), f, Explosion.Mode.BREAK);
	}

	protected void writeAdditional(CompoundNBT compound)
	{
		compound.putShort("Fuse", (short) this.getFuse());
	}

	protected void readAdditional(CompoundNBT compound)
	{
		this.setFuse(compound.getShort("Fuse"));
	}

	@Nullable
	public LivingEntity getTntPlacedBy()
	{
		return this.tntPlacedBy;
	}

	protected float getEyeHeight(Pose poseIn, EntitySize sizeIn)
	{
		return 0.0F;
	}

	public void setFuse(int fuseIn)
	{
		this.dataManager.set(FUSE, fuseIn);
		this.fuse = fuseIn;
	}

	public void notifyDataManagerChange(DataParameter<?> key)
	{
		if (FUSE.equals(key))
		{
			this.fuse = this.getFuseDataManager();
		}
	}

	public int getFuseDataManager()
	{
		return this.dataManager.get(FUSE);
	}

	public int getFuse()
	{
		return this.fuse;
	}

	@Override
	public IPacket<?> createSpawnPacket()
	{
		return NetworkHooks.getEntitySpawningPacket(this);
	}
}