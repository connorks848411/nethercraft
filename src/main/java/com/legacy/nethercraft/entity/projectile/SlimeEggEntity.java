package com.legacy.nethercraft.entity.projectile;

import com.legacy.nethercraft.entity.NetherEntityTypes;
import com.legacy.nethercraft.entity.LavaSlimeEntity;
import com.legacy.nethercraft.item.NetherItems;

import net.minecraft.block.Blocks;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.monster.BlazeEntity;
import net.minecraft.entity.projectile.ProjectileItemEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.network.IPacket;
import net.minecraft.particles.IParticleData;
import net.minecraft.particles.ItemParticleData;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.network.FMLPlayMessages;
import net.minecraftforge.fml.network.NetworkHooks;

public class SlimeEggEntity extends ProjectileItemEntity
{

	public SlimeEggEntity(EntityType<? extends SlimeEggEntity> p_i50159_1_, World p_i50159_2_)
	{
		super(p_i50159_1_, p_i50159_2_);
	}

	public SlimeEggEntity(World worldIn, LivingEntity throwerIn)
	{
		super(NetherEntityTypes.SLIME_EGGS, throwerIn, worldIn);
	}

	public SlimeEggEntity(World worldIn, double x, double y, double z)
	{
		super(NetherEntityTypes.SLIME_EGGS, x, y, z, worldIn);
	}

	public SlimeEggEntity(FMLPlayMessages.SpawnEntity spawnEntity, World world)
	{
		this(NetherEntityTypes.SLIME_EGGS, world);
	}

	@Override
	protected Item getDefaultItem()
	{
		return NetherItems.slime_eggs;
	}

	@OnlyIn(Dist.CLIENT)
	private IParticleData func_213887_n()
	{
		return new ItemParticleData(ParticleTypes.ITEM, new ItemStack(NetherItems.slime_eggs));
	}

	@OnlyIn(Dist.CLIENT)
	public void handleStatusUpdate(byte id)
	{
		if (id == 3)
		{
			IParticleData iparticledata = this.func_213887_n();
			for (int i = 0; i < 8; ++i)
			{
				this.world.addParticle(iparticledata, this.getPosition().getX(), this.getPosition().getY(), this.getPosition().getZ(), 0.0D, 0.0D, 0.0D);
			}
		}
	}

	@SuppressWarnings("deprecation")
	protected void onImpact(RayTraceResult result)
	{
		if (result.getType() == RayTraceResult.Type.ENTITY)
		{
			Entity entity = ((EntityRayTraceResult) result).getEntity();
			int i = entity instanceof BlazeEntity ? 3 : 0;
			entity.attackEntityFrom(DamageSource.causeThrownDamage(this, this.getThrower()), (float) i);
		}

		if (result.getType() == RayTraceResult.Type.BLOCK && !this.world.isRemote)
		{
			if (this.rand.nextInt(6) == 0)
			{
				int size = 1;

				if (this.rand.nextInt(6) == 0)
				{
					size = 3;
				}

				for (int slime = 0; slime < size; ++slime)
				{
					LavaSlimeEntity netherSlime = new LavaSlimeEntity(NetherEntityTypes.LAVA_SLIME, this.world);
					netherSlime.setPositionAndRotation(this.getPosition().getX(), this.getPosition().getY(), this.getPosition().getZ(), this.rotationYaw, this.rotationPitch);
					this.world.addEntity(netherSlime);
				}
			}

			int x = MathHelper.floor(result.getHitVec().x);
			int y = MathHelper.floor(result.getHitVec().y);
			int z = MathHelper.floor(result.getHitVec().z);
			BlockPos pos = new BlockPos(x, y, z);

			if (this.world.isAirBlock(pos) && Blocks.FIRE.isValidPosition(Blocks.FIRE.getDefaultState(), this.world, pos))
			{
				this.world.setBlockState(pos, Blocks.FIRE.getDefaultState());
			}
			else if (this.world.isAirBlock(pos) && Blocks.FIRE.isValidPosition(Blocks.FIRE.getDefaultState(), this.world, pos.up()))
			{
				this.world.setBlockState(pos.up(), Blocks.FIRE.getDefaultState());
			}

			this.world.addParticle(ParticleTypes.SMOKE, this.getPosition().getX(), this.getPosition().getY(), this.getPosition().getY(), 0.0D, 0.0D, 0.0D);

			this.remove();
		}

		if (!this.world.isRemote)
		{
			this.world.setEntityState(this, (byte) 3);
			this.remove();
		}
	}

	@Override
	public IPacket<?> createSpawnPacket()
	{
		return NetworkHooks.getEntitySpawningPacket(this);
	}
}