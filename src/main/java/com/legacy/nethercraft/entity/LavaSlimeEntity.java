package com.legacy.nethercraft.entity;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.monster.SlimeEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.world.World;

public class LavaSlimeEntity extends SlimeEntity
{

    public static final DataParameter<Integer> SLIME_TYPE = EntityDataManager.<Integer>createKey(LavaSlimeEntity.class, DataSerializers.VARINT);

	public LavaSlimeEntity(EntityType<? extends LavaSlimeEntity> type, World worldIn) 
	{
		super(type, worldIn);

        this.dataManager.set(SLIME_TYPE, this.rand.nextInt(2));
	}

	@Override
    protected boolean spawnCustomParticles() 
	{
		return true; 
	}

	@SuppressWarnings("unchecked")
	public EntityType<? extends LavaSlimeEntity> getType()
	{
		return (EntityType<? extends LavaSlimeEntity>) super.getType();
	}

	@Override
    protected void registerData()
    {
        super.registerData();
        
        this.dataManager.register(SLIME_TYPE, Integer.valueOf(this.rand.nextInt(2)));
    }

	@Override
    public void writeAdditional(CompoundNBT compound)
    {
        super.writeAdditional(compound);

        compound.putInt("slime_type", this.dataManager.get(SLIME_TYPE));
    }

	@Override
    public void readAdditional(CompoundNBT compound)
    {
    	super.readAdditional(compound);

    	this.dataManager.set(SLIME_TYPE, compound.getInt("slime_type"));
    }

	public void remove()
	{
		int i = this.getSlimeSize();
		if (!this.world.isRemote && i > 1 && this.getHealth() <= 0.0F)
		{
			int j = 2 + this.rand.nextInt(3);
			for (int k = 0; k < j; ++k)
			{
				float f = ((float) (k % 2) - 0.5F) * (float) i / 4.0F;
				float f1 = ((float) (k / 2) - 0.5F) * (float) i / 4.0F;
				LavaSlimeEntity entityslime = this.getType().create(this.world);
				if (this.hasCustomName())
				{
					entityslime.setCustomName(this.getCustomName());
				}
				if (this.isNoDespawnRequired())
				{
					entityslime.enablePersistence();
				}
				entityslime.setSlimeSize(i / 2, true);
				entityslime.setLocationAndAngles(this.getPosition().getX() + (double) f, this.getPosition().getY() + 0.5D, this.getPosition().getZ() + (double) f1, this.rand.nextFloat() * 360.0F, 0.0F);
				this.world.addEntity(entityslime);
			}
		}
		super.remove();
	}

}