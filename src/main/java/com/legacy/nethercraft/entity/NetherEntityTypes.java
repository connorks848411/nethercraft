package com.legacy.nethercraft.entity;

import java.util.Random;

import com.legacy.nethercraft.Nethercraft;
import com.legacy.nethercraft.NethercraftRegistry;
import com.legacy.nethercraft.block.NetherBlocks;
import com.legacy.nethercraft.entity.misc.GhastBombEntity;
import com.legacy.nethercraft.entity.misc.LavaBoatEntity;
import com.legacy.nethercraft.entity.misc.NetherPaintingEntity;
import com.legacy.nethercraft.entity.projectile.LiniumArrowEntity;
import com.legacy.nethercraft.entity.projectile.NeridiumArrowEntity;
import com.legacy.nethercraft.entity.projectile.NetherrackArrowEntity;
import com.legacy.nethercraft.entity.projectile.PyridiumArrowEntity;
import com.legacy.nethercraft.entity.projectile.SlimeEggEntity;
import com.legacy.nethercraft.entity.tribal.TribalArcherEntity;
import com.legacy.nethercraft.entity.tribal.TribalTraineeEntity;
import com.legacy.nethercraft.entity.tribal.TribalWarriorEntity;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntitySpawnPlacementRegistry;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.SpawnReason;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.Difficulty;
import net.minecraft.world.IWorld;
import net.minecraft.world.gen.Heightmap;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.registries.ObjectHolder;

@ObjectHolder(Nethercraft.MODID)
public class NetherEntityTypes
{

	public static final EntityType<ImpEntity> IMP = buildEntity("imp", EntityType.Builder.create(ImpEntity::new, EntityClassification.CREATURE).immuneToFire().size(1.0F, 1.8F));

	public static final EntityType<DarkZombieEntity> DARK_ZOMBIE = buildEntity("dark_zombie", EntityType.Builder.create(DarkZombieEntity::new, EntityClassification.MONSTER).size(0.6F, 1.95F));
	public static final EntityType<BloodyZombieEntity> BLOODY_ZOMBIE = buildEntity("bloody_zombie", EntityType.Builder.create(BloodyZombieEntity::new, EntityClassification.MONSTER).size(0.6F, 1.95F));
	public static final EntityType<LavaSlimeEntity> LAVA_SLIME = buildEntity("lava_slime", EntityType.Builder.create(LavaSlimeEntity::new, EntityClassification.MONSTER).immuneToFire().size(2.04F, 2.04F));
	public static final EntityType<CamouflageSpiderEntity> CAMOUFLAGE_SPIDER = buildEntity("camouflage_spider", EntityType.Builder.create(CamouflageSpiderEntity::new, EntityClassification.MONSTER).immuneToFire().size(1.4F, 0.9F));

	public static final EntityType<TribalWarriorEntity> TRIBAL_WARRIOR = buildEntity("tribal_warrior", EntityType.Builder.create(TribalWarriorEntity::new, EntityClassification.MONSTER).immuneToFire().size(0.6F, 1.95F));
	public static final EntityType<TribalArcherEntity> TRIBAL_ARCHER = buildEntity("tribal_archer", EntityType.Builder.create(TribalArcherEntity::new, EntityClassification.MONSTER).immuneToFire().size(0.6F, 1.95F));
	public static final EntityType<TribalTraineeEntity> TRIBAL_TRAINEE = buildEntity("tribal_trainee", EntityType.Builder.create(TribalTraineeEntity::new, EntityClassification.MONSTER).immuneToFire().size(0.4F, 1.3F));

	public static final EntityType<NetherrackArrowEntity> NETHERRACK_ARROW = buildEntity("netherrack_arrow", EntityType.Builder.<NetherrackArrowEntity>create(NetherrackArrowEntity::new, EntityClassification.MISC).setCustomClientFactory(NetherrackArrowEntity::new).setShouldReceiveVelocityUpdates(true).size(0.5F, 0.5F));
	public static final EntityType<NeridiumArrowEntity> NERIDIUM_ARROW = buildEntity("neridium_arrow", EntityType.Builder.<NeridiumArrowEntity>create(NeridiumArrowEntity::new, EntityClassification.MISC).setCustomClientFactory(NeridiumArrowEntity::new).setShouldReceiveVelocityUpdates(true).size(0.5F, 0.5F));
	public static final EntityType<PyridiumArrowEntity> PYRIDIUM_ARROW = buildEntity("pyridium_arrow", EntityType.Builder.<PyridiumArrowEntity>create(PyridiumArrowEntity::new, EntityClassification.MISC).setCustomClientFactory(PyridiumArrowEntity::new).setShouldReceiveVelocityUpdates(true).size(0.5F, 0.5F));
	public static final EntityType<LiniumArrowEntity> LINIUM_ARROW = buildEntity("linium_arrow", EntityType.Builder.<LiniumArrowEntity>create(LiniumArrowEntity::new, EntityClassification.MISC).setCustomClientFactory(LiniumArrowEntity::new).setShouldReceiveVelocityUpdates(true).size(0.5F, 0.5F));
	public static final EntityType<SlimeEggEntity> SLIME_EGGS = buildEntity("slime_eggs", EntityType.Builder.<SlimeEggEntity>create(SlimeEggEntity::new, EntityClassification.MISC).setCustomClientFactory(SlimeEggEntity::new).setShouldReceiveVelocityUpdates(true).size(0.25F, 0.25F));
	public static final EntityType<GhastBombEntity> GHAST_BOMB = buildEntity("ghast_bomb", EntityType.Builder.<GhastBombEntity>create(GhastBombEntity::new, EntityClassification.MISC).setCustomClientFactory(GhastBombEntity::new).setShouldReceiveVelocityUpdates(true).immuneToFire().size(0.98F, 0.98F));
	public static final EntityType<NetherPaintingEntity> NETHER_PAINTING = buildEntity("nether_painting", EntityType.Builder.<NetherPaintingEntity>create(NetherPaintingEntity::new, EntityClassification.MISC).setCustomClientFactory(NetherPaintingEntity::new).immuneToFire().size(0.5F, 0.5F));
	public static final EntityType<LavaBoatEntity> LAVA_BOAT = buildEntity("lava_boat", EntityType.Builder.<LavaBoatEntity>create(LavaBoatEntity::new, EntityClassification.MISC).setCustomClientFactory(LavaBoatEntity::new).setShouldReceiveVelocityUpdates(true).immuneToFire().size(1.375F, 0.5625F));

	public static void init(Register<EntityType<?>> event)
	{
		NethercraftRegistry.register(event.getRegistry(), "imp", IMP);
		NethercraftRegistry.register(event.getRegistry(), "dark_zombie", DARK_ZOMBIE);
		NethercraftRegistry.register(event.getRegistry(), "bloody_zombie", BLOODY_ZOMBIE);
		NethercraftRegistry.register(event.getRegistry(), "lava_slime", LAVA_SLIME);
		NethercraftRegistry.register(event.getRegistry(), "camouflage_spider", CAMOUFLAGE_SPIDER);
		NethercraftRegistry.register(event.getRegistry(), "tribal_warrior", TRIBAL_WARRIOR);
		NethercraftRegistry.register(event.getRegistry(), "tribal_archer", TRIBAL_ARCHER);
		NethercraftRegistry.register(event.getRegistry(), "tribal_trainee", TRIBAL_TRAINEE);
		NethercraftRegistry.register(event.getRegistry(), "netherrack_arrow", NETHERRACK_ARROW);
		NethercraftRegistry.register(event.getRegistry(), "neridium_arrow", NERIDIUM_ARROW);
		NethercraftRegistry.register(event.getRegistry(), "pyridium_arrow", PYRIDIUM_ARROW);
		NethercraftRegistry.register(event.getRegistry(), "linium_arrow", LINIUM_ARROW);
		NethercraftRegistry.register(event.getRegistry(), "slime_eggs", SLIME_EGGS);
		NethercraftRegistry.register(event.getRegistry(), "ghast_bomb", GHAST_BOMB);
		NethercraftRegistry.register(event.getRegistry(), "nether_painting", NETHER_PAINTING);
		NethercraftRegistry.register(event.getRegistry(), "lava_boat", LAVA_BOAT);

		registerSpawnConditions();
	}

	private static <T extends Entity> EntityType<T> buildEntity(String key, EntityType.Builder<T> builder)
	{
		return builder.build(Nethercraft.find(key));
	}

	public static boolean canSpawn(EntityType<? extends MobEntity> type, IWorld world, SpawnReason reason, BlockPos pos, Random random)
	{
		return world.getDifficulty() != Difficulty.PEACEFUL;
	}

	public static boolean canSpawnSand(EntityType<? extends MobEntity> type, IWorld world, SpawnReason reason, BlockPos pos, Random random)
	{
		return world.getDifficulty() != Difficulty.PEACEFUL && world.getBlockState(pos.down()).getBlock() == NetherBlocks.heat_sand;
	}

	private static void registerSpawnConditions()
	{
		EntitySpawnPlacementRegistry.register(NetherEntityTypes.IMP, EntitySpawnPlacementRegistry.PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, NetherEntityTypes::canSpawn);
		EntitySpawnPlacementRegistry.register(NetherEntityTypes.DARK_ZOMBIE, EntitySpawnPlacementRegistry.PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, NetherEntityTypes::canSpawn);
		EntitySpawnPlacementRegistry.register(NetherEntityTypes.BLOODY_ZOMBIE, EntitySpawnPlacementRegistry.PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, NetherEntityTypes::canSpawn);
		EntitySpawnPlacementRegistry.register(NetherEntityTypes.LAVA_SLIME, EntitySpawnPlacementRegistry.PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, NetherEntityTypes::canSpawn);
		EntitySpawnPlacementRegistry.register(NetherEntityTypes.CAMOUFLAGE_SPIDER, EntitySpawnPlacementRegistry.PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, NetherEntityTypes::canSpawn);
		EntitySpawnPlacementRegistry.register(NetherEntityTypes.TRIBAL_WARRIOR, EntitySpawnPlacementRegistry.PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, NetherEntityTypes::canSpawn);
		EntitySpawnPlacementRegistry.register(NetherEntityTypes.TRIBAL_ARCHER, EntitySpawnPlacementRegistry.PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, NetherEntityTypes::canSpawn);
		EntitySpawnPlacementRegistry.register(NetherEntityTypes.TRIBAL_TRAINEE, EntitySpawnPlacementRegistry.PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, NetherEntityTypes::canSpawn);
	}
}
