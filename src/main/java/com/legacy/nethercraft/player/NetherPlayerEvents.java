package com.legacy.nethercraft.player;

import java.util.List;
import java.util.Map;

import com.google.common.collect.ImmutableMap.Builder;
import com.legacy.nethercraft.block.NetherBlocks;
import com.legacy.nethercraft.entity.misc.LavaBoatEntity;
import com.legacy.nethercraft.item.NetherItems;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.RotatedPillarBlock;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.AxeItem;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Direction;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RayTraceContext;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.living.LivingAttackEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.entity.player.UseHoeEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class NetherPlayerEvents
{

	public static final Map<Block, BlockState> HOE_LOOKUP = (new Builder<Block, BlockState>()).put(NetherBlocks.nether_dirt, NetherBlocks.nether_farmland.getDefaultState()).build();

	private int burnTime;

	@SubscribeEvent
	public void onPlayerRightClickAxe(PlayerInteractEvent.RightClickItem event)
	{
		if (event.getItemStack().getItem() instanceof AxeItem)
		{
			PlayerEntity player = event.getPlayer();
			World world = event.getWorld();
			BlockRayTraceResult rayTraceResult = (BlockRayTraceResult) rayTrace(world, player);
			BlockPos pos = rayTraceResult.getPos();
			BlockState blockState = world.getBlockState(pos);
			if (blockState.getBlock() == NetherBlocks.glowood_log)
			{
				player.getEntityWorld().playSound(player, pos, SoundEvents.ITEM_AXE_STRIP, SoundCategory.BLOCKS, 1.0F, 1.0F);
				player.swingArm(event.getHand());
				if (!world.isRemote)
				{
					world.setBlockState(pos, NetherBlocks.stripped_glowood_log.getDefaultState().with(RotatedPillarBlock.AXIS, blockState.get(RotatedPillarBlock.AXIS)), 11);
					if (player != null)
					{
						event.getItemStack().damageItem(1, player, (playerEntity) ->
						{
							playerEntity.sendBreakAnimation(event.getHand());
						});
					}
				}
			}
		}
	}

	@SubscribeEvent
	public void onPlayerRightClickHoe(UseHoeEvent event)
	{
		PlayerEntity player = event.getPlayer();
		World world = event.getContext().getWorld();
		BlockPos pos = event.getContext().getPos();
		BlockState dirtState = world.getBlockState(pos);
		BlockState resultState = HOE_LOOKUP.get(dirtState.getBlock());
		if (resultState != null && event.getContext().getFace() != Direction.DOWN && world.isAirBlock(pos.up()))
		{
			if (world.rand.nextInt(10) == 0)
			{
				ItemEntity itementity = new ItemEntity(world, pos.getX(), pos.getY() + 1, pos.getZ(), new ItemStack(NetherItems.dark_seeds));
				itementity.setDefaultPickupDelay();
				world.addEntity(itementity);
			}

			player.getEntityWorld().playSound(player, pos, SoundEvents.ITEM_HOE_TILL, SoundCategory.BLOCKS, 1.0F, 1.0F);
			player.swingArm(event.getContext().getHand());
			if (!world.isRemote)
			{
				world.setBlockState(pos, resultState, 11);
				if (player != null)
				{
					event.getContext().getItem().damageItem(1, player, (playerEntity) ->
					{
						playerEntity.sendBreakAnimation(event.getContext().getHand());
					});
				}
			}
			event.setCanceled(true);
		}
	}

	protected static RayTraceResult rayTrace(World worldIn, PlayerEntity player)
	{
		float f = player.rotationPitch;
		float f1 = player.rotationYaw;
		Vec3d vec3d = player.getEyePosition(1.0F);
		float f2 = MathHelper.cos(-f1 * ((float) Math.PI / 180F) - (float) Math.PI);
		float f3 = MathHelper.sin(-f1 * ((float) Math.PI / 180F) - (float) Math.PI);
		float f4 = -MathHelper.cos(-f * ((float) Math.PI / 180F));
		float f5 = MathHelper.sin(-f * ((float) Math.PI / 180F));
		float f6 = f3 * f4;
		float f7 = f2 * f4;
		double d0 = player.getAttribute(PlayerEntity.REACH_DISTANCE).getValue();
		Vec3d vec3d1 = vec3d.add((double) f6 * d0, (double) f5 * d0, (double) f7 * d0);
		return worldIn.rayTraceBlocks(new RayTraceContext(vec3d, vec3d1, RayTraceContext.BlockMode.OUTLINE, RayTraceContext.FluidMode.SOURCE_ONLY, player));
	}

	@SubscribeEvent
	public void onPlayerHurt(LivingAttackEvent event)
	{
		if (event.getEntityLiving().getRidingEntity() instanceof LavaBoatEntity && event.getSource().isFireDamage())
		{
			event.getEntityLiving().extinguish();
			event.setCanceled(true);
			this.burnTime = 40;
		}

		List<LavaBoatEntity> list = event.getEntityLiving().world.<LavaBoatEntity>getEntitiesWithinAABB(LavaBoatEntity.class, event.getEntityLiving().getBoundingBox().grow(0.5F));

		if (this.burnTime > 0 && event.getSource().isFireDamage() && event.getEntityLiving().getRidingEntity() == null && !list.isEmpty())
		{
			event.getEntityLiving().extinguish();
			event.setCanceled(true);
		}
	}

	@SubscribeEvent
	public void onEntityUpdate(LivingUpdateEvent event)
	{
		if (event.getEntityLiving() instanceof PlayerEntity && this.burnTime > 0 && event.getEntityLiving().getRidingEntity() == null)
		{
			--this.burnTime;
		}
	}
}
