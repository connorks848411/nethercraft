package com.legacy.nethercraft;

import com.legacy.nethercraft.client.NethercraftClient;
import com.legacy.nethercraft.item.util.FuelHandler;
import com.legacy.nethercraft.player.NetherPlayerEvents;
import com.legacy.nethercraft.world.NetherEvents;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

@Mod("nethercraft")
public class Nethercraft
{

	public static final String MODID = "nethercraft";

	public static ResourceLocation locate(String name)
	{
		return new ResourceLocation(MODID, name);
	}

	public static String find(String name)
	{
		return new String(MODID + ":" + name);
	}

	public Nethercraft()
	{
		FMLJavaModLoadingContext.get().getModEventBus().addListener(this::initialization);
		FMLJavaModLoadingContext.get().getModEventBus().addListener(NetherEvents::setupNetherFeatures);
		DistExecutor.runWhenOn(Dist.CLIENT, () -> () -> FMLJavaModLoadingContext.get().getModEventBus().addListener(NethercraftClient::initialization));
	}

	private void initialization(final FMLCommonSetupEvent event)
	{
		MinecraftForge.EVENT_BUS.register(new NetherEvents());
		MinecraftForge.EVENT_BUS.register(new FuelHandler());
		MinecraftForge.EVENT_BUS.register(new NetherPlayerEvents());
	}
}
